﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPT.service.exception
{
    class UtilisateurException : Exception
    {
        private string msg;

        public string Msg
        {
            get { return msg; }
            set { msg = value; }
        }
        public UtilisateurException(int indice)
        {
            switch (indice)
            {
                case 1: this.Msg = "Login et/ou mot de passe erroné"; break;
                case 2: this.Msg = "Login et/ou mot de passe erroné"; break;
            }
        }
    }
}
