﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TPT.model;
using TPT.model.liaison_classe;

namespace TPT.service
{
    class ClasseService
    {
        string url = Util.getUrl() + "classe";
        public List<Classe> getClasseFromAPi()
        {
            var client = new RestClient(url);
            List<Classe> result = new List<Classe>();
            IRestResponse response = client.Execute(new RestRequest());
            var content = response.Content;
            JArray releases = JArray.Parse(content);
            List<Classe> liste = releases.ToObject<List<Classe>>();
            return liste;
        }
        public List<Classe> getAllClasses()
        {
            List<Classe> reponse = getClasseFromAPi(); 
            return reponse;
        }

        public int getNbreEtu(List<Eleve> liste)
        {
            return liste.Count();
        }
        public List<Eleve> getEleveByIdClass(int idClass)
        {
            string url = Util.getUrl() + "classe/" + idClass;
            var client = new RestClient(url);
            List<Eleve> result = new List<Eleve>();
            IRestResponse response = client.Execute(new RestRequest());
            var content = response.Content;
            JArray releases = JArray.Parse(content);
            List<Eleve> liste = releases.ToObject<List<Eleve>>();
            return liste;
        }
        public List<Eleve> getAllEleve()
        {
            List<Eleve> reponse = new List<Eleve>();
            return reponse;
        }

        public List<Filiere> getAllFiliere()
        {
            List<Filiere> reponse = new List<Filiere>();
            Filiere f1 = new Filiere(1, "filiere1", 150, "codefiliere1");
            Filiere f2 = new Filiere(2, "filiere2", 250, "codefiliere3");
            Filiere f3 = new Filiere(3, "filiere3", 450, "codefiliere3");
            reponse.Add(f1);
            reponse.Add(f2);
            reponse.Add(f3);
            return reponse;
        }

        public List<Liaison_Prof_Classe> getAllLiaison()
        {
            List<Liaison_Prof_Classe> reponse = new List<Liaison_Prof_Classe>();
            Liaison_Prof_Classe l1 = new Liaison_Prof_Classe(1, 1);
            Liaison_Prof_Classe l2 = new Liaison_Prof_Classe(1, 2);
            Liaison_Prof_Classe l3 = new Liaison_Prof_Classe(1, 5);
            Liaison_Prof_Classe l4 = new Liaison_Prof_Classe(3, 1);
            Liaison_Prof_Classe l5 = new Liaison_Prof_Classe(1, 2);
            Liaison_Prof_Classe l6 = new Liaison_Prof_Classe(1, 5);
            reponse.Add(l1);
            reponse.Add(l2);
            reponse.Add(l3);
            reponse.Add(l4);
            reponse.Add(l5);
            reponse.Add(l6);
            return reponse;
        }

        public void insertDonneListeETU(List<Eleve> listeDonnee, DataGridView data)
        {
            try
            {
                data.Rows.Clear() ;
                foreach (Eleve liste in listeDonnee)
                {
                    DataGridViewRow dgvRow = new DataGridViewRow();
                    DataGridViewCell dgvCell = default(DataGridViewCell);

                    dgvCell = new DataGridViewTextBoxCell();
                    dgvCell.Value = liste.IdEtudiants;
                    dgvRow.Cells.Add(dgvCell);

                    dgvCell = new DataGridViewTextBoxCell();
                    dgvCell.Value = liste.Nom;
                    dgvRow.Cells.Add(dgvCell);

                    dgvCell = new DataGridViewTextBoxCell();
                    dgvCell.Value = liste.Prenom;
                    dgvRow.Cells.Add(dgvCell);

                    data.Rows.Add(dgvRow);
                }
            }
            catch (Exception) { Console.WriteLine("ato @erreur"); }
        }
    }
}
