﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TPT.model;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace TPT.service.exception
{
    class MatiereService
    {
        public List<Matiere> allMatiere()
        {
            List<Matiere> resultat = new List<Matiere>();
            Matiere m1 = new Matiere(1, "M1", 30, 1);
            Matiere m2 = new Matiere(2, "M2", 10, 1);
            Matiere m3 = new Matiere(3, "M3", 20, 1);
            resultat.Add(m1);
            resultat.Add(m2);
            resultat.Add(m3);
            return resultat;
        }

        public Matiere getMatiereById(int idMatiere)
        {
            string url = Util.getUrl() + "matiere/" + idMatiere;
            var client = new RestClient(url);
            List<Matiere> result = new List<Matiere>();
            IRestResponse response = client.Execute(new RestRequest());
            var content = response.Content;
            JArray releases = JArray.Parse(content);
            List<Matiere> liste = releases.ToObject<List<Matiere>>();
            return liste[0];
        }
    }
}
