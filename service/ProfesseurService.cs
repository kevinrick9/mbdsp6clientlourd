﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TPT.model;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace TPT.service
{
    class ProfesseurService
    {
        public static Professeurs getOneProf(Utilisateur user)
        {
            Professeurs result = new Professeurs();
            string url = Util.getUrl() + "prof/" + user.Id;
            var client = new RestClient(url);
            IRestResponse response = client.Execute(new RestRequest());
            var content = response.Content;
            JArray releases = JArray.Parse(content);
            List<Professeurs> liste = releases.ToObject<List<Professeurs>>();
            //Console.WriteLine("ato professeurservice fonction getoneprof\nla photo : "+liste[0].Photo);
            return liste[0];
        }

        public void insertDonneListeETU(List<Utilisateur> listeDonnee, DataGridView data)
        {
            try
            {
                data.Rows.Clear();
                foreach (Utilisateur liste in listeDonnee)
                {
                    DataGridViewRow dgvRow = new DataGridViewRow();
                    DataGridViewCell dgvCell = default(DataGridViewCell);

                    dgvCell = new DataGridViewTextBoxCell();
                    dgvCell.Value = liste.Nom;
                    dgvRow.Cells.Add(dgvCell);

                    dgvCell = new DataGridViewTextBoxCell();
                    dgvCell.Value = liste.Prenom;
                    dgvRow.Cells.Add(dgvCell);

                    dgvCell = new DataGridViewTextBoxCell();
                    dgvCell.Value = liste.Adresse;
                    dgvRow.Cells.Add(dgvCell);

                    data.Rows.Add(dgvRow);
                }
            }
            catch (Exception) { Console.WriteLine("ato @erreur"); }
        }
    }
}
