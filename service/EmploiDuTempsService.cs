﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TPT.model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System.Windows.Forms;
using TPT.service.exception;

namespace TPT.service
{
    class EmploiDuTempsService
    {
        public List<string> jourSemaine()
        {
            List<string> result = new List<string>();
            result.Add("0");
            result.Add("Lundi");
            result.Add("Mardi");
            result.Add("Mercredi");
            result.Add("Jeudi");
            result.Add("Vendredi");
            result.Add("Samedi");
            result.Add("Dimanche");
            return result;
        }
        public List<EmploiDuTemps> getEmploiDuTempsByIdUser(int idUser)
        {
            string url = Util.getUrl() + "emploi_du_temps/" + idUser;
            var client = new RestClient(url);
            List<EmploiDuTemps> result = new List<EmploiDuTemps>();
            IRestResponse response = client.Execute(new RestRequest());
            var content = response.Content;
            JArray releases = null;
            List<EmploiDuTemps> liste = new List<EmploiDuTemps>();
            try
            {
                releases = JArray.Parse(content);
                liste = releases.ToObject<List<EmploiDuTemps>>();
            }
            catch
            {

            }
            return liste;
        }

        public List<string> getDateDebutEtFinSemaine(string date)
        {
            string url = Util.getUrl() + "emploi_du_temps/date/" + date;
            var client = new RestClient(url);
            List<string> result = new List<string>();
            IRestResponse response = client.Execute(new RestRequest());
            var content = response.Content;
            JArray releases = null;
            List<string> liste = new List<string>();
            try
            {
                releases = JArray.Parse(content);
                liste = releases.ToObject<List<string>>();
            }
            catch
            {

            }
            return liste;
        }

        public void insertDonneListeEmploiDuTemps(List<EmploiDuTemps> listeDonnee, DataGridView data)
        {
            try
            {
                data.Rows.Clear();
                foreach (EmploiDuTemps liste in listeDonnee)
                {
                    DataGridViewRow dgvRow = new DataGridViewRow();
                    DataGridViewCell dgvCell = default(DataGridViewCell);

                    dgvCell = new DataGridViewTextBoxCell();
                    dgvCell.Value =jourSemaine()[liste.Jour];
                    dgvRow.Cells.Add(dgvCell);

                    dgvCell = new DataGridViewTextBoxCell();
                    dgvCell.Value = liste.HeureDebut;
                    dgvRow.Cells.Add(dgvCell);

                    dgvCell = new DataGridViewTextBoxCell();
                    dgvCell.Value = liste.HeureFin;
                    dgvRow.Cells.Add(dgvCell);

                    Matiere matiere = new MatiereService().getMatiereById(liste.IdMatiere);
                    dgvCell = new DataGridViewTextBoxCell();
                    dgvCell.Value = matiere.NomMatiere;
                    dgvRow.Cells.Add(dgvCell);

                    data.Rows.Add(dgvRow);
                }
            }
            catch (Exception) { Console.WriteLine("ato @erreur"); }
        }

    }
}
