﻿using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TPT.model;

namespace TPT.service
{
    class CoursService
    {
        public List<Cours> getTousCours()
        {
            List<Cours> result = new List<Cours>();

            /*Cours cours1 = new Cours(1, 1, 1, 1, "Cours1", "url", "15/05/2020", "09:10:00");
            Cours cours2 = new Cours(2, 2, 1, 2, "Cours2","url", "15/05/2020", "09:10:00");
            result.Add(cours1);
            result.Add(cours2);*/
            return result;
        }

        public List<Cours> getCoursByIdProf(int idProf)
        {
            string url = Util.getUrl() + "cours/prof/" + idProf;
            var client = new RestClient(url);
            List<Cours> result = new List<Cours>();
            IRestResponse response = client.Execute(new RestRequest());
            var content = response.Content;
            JArray releases = JArray.Parse(content);
            List<Cours> liste = releases.ToObject<List<Cours>>();
            return liste;
        }

        public void insertDonneListeCours(List<Cours> listeDonnee, DataGridView data)
        {
            try
            {
                data.Rows.Clear();
                foreach (Cours liste in listeDonnee)
                {
                    DataGridViewRow dgvRow = new DataGridViewRow();
                    DataGridViewCell dgvCell = default(DataGridViewCell);

                    dgvCell = new DataGridViewTextBoxCell();
                    dgvCell.Value = liste.NomCours;
                    dgvRow.Cells.Add(dgvCell);

                    dgvCell = new DataGridViewTextBoxCell();
                    dgvCell.Value = liste.UrlCours;
                    dgvRow.Cells.Add(dgvCell);

                    dgvCell = new DataGridViewTextBoxCell();
                    dgvCell.Value = liste.DateUpload;
                    dgvRow.Cells.Add(dgvCell);

                    dgvCell = new DataGridViewTextBoxCell();
                    dgvCell.Value = liste.TypeCours;
                    dgvRow.Cells.Add(dgvCell);

                    data.Rows.Add(dgvRow);
                }
            }
            catch (Exception) { Console.WriteLine("ato @erreur"); }
        }
    }
}
