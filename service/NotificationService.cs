﻿using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TPT.model;

namespace TPT.service
{
    class NotificationService
    {
        public List<Notification> getAllNotifFromAPi()
        {
            string url = Util.getUrl()+"notification/";
            var client = new RestClient(url);
            List<Notification> result = new List<Notification>();
            IRestResponse response = client.Execute(new RestRequest());
            var content = response.Content;
            JArray releases = null;
            List<Notification> liste = new List<Notification>();
            try
            {
                releases = JArray.Parse(content);
                liste = releases.ToObject<List<Notification>>();
            }
            catch
            {

            }
            return liste;
        }

        public List<Notification> getAllNotifByDateFromAPi(string date)
        {
            //Console.WriteLine("la date obtenue : "+date);
            string url = Util.getUrl()+"notification/"+date;
            var client = new RestClient(url);
            List<Notification> result = new List<Notification>();
            IRestResponse response = client.Execute(new RestRequest());
            var content = response.Content;
            JArray releases = null;
            List<Notification> liste = new List<Notification>();
            try
            {
                releases = JArray.Parse(content);
                liste = releases.ToObject<List<Notification>>();
            }
            catch
            {

            }
            return liste;
        }


        public void insertDonneListeNotif(List<Notification> listeDonnee, DataGridView data)
        {
            try
            {
                data.Rows.Clear();
                foreach (Notification liste in listeDonnee)
                {
                    DataGridViewRow dgvRow = new DataGridViewRow();
                    DataGridViewCell dgvCell = default(DataGridViewCell);

                    if (liste.IdSender==0)
                    {
                        dgvCell = new DataGridViewTextBoxCell();
                        dgvCell.Value = "Tous les élèves";
                        dgvRow.Cells.Add(dgvCell);
                    }
                    else
                    {
                        Utilisateur user = new UtilisateurService().getUtilisateurById(liste.IdSender.ToString());
                        dgvCell = new DataGridViewTextBoxCell();
                        dgvCell.Value = user.Nom+" "+user.Prenom;
                        dgvRow.Cells.Add(dgvCell);
                    }                    

                    dgvCell = new DataGridViewTextBoxCell();
                    dgvCell.Value = liste.Libelle;
                    dgvRow.Cells.Add(dgvCell);

                    dgvCell = new DataGridViewTextBoxCell();
                    dgvCell.Value = liste.DateNotif;
                    dgvRow.Cells.Add(dgvCell);

                    data.Rows.Add(dgvRow);
                }
            }
            catch (Exception) { Console.WriteLine("ato @erreur"); }
        }

        public List<Messages> getMessageByIdDest(int iddest)
        {
            string url = Util.getUrl()+"message/reception/";
            var client = new RestClient(url);
            IRestResponse response = client.Execute(new RestRequest());
            var content = response.Content;
            JArray releases = null;
            List<Messages> liste = new List<Messages>();
            try
            {
                releases = JArray.Parse(content);
                liste = releases.ToObject<List<Messages>>();
            }
            catch
            {

            }
            
            return liste;
        }

        public void insertDonneListeMsgBYIdDest(List<Messages> listeDonnee, DataGridView data)
        {
            try
            {
                data.Rows.Clear();
                foreach (Messages liste in listeDonnee)
                {
                    DataGridViewRow dgvRow = new DataGridViewRow();
                    DataGridViewCell dgvCell = default(DataGridViewCell);

                    Utilisateur user = new UtilisateurService().getUtilisateurById(liste.IdSource.ToString());
                    dgvCell = new DataGridViewTextBoxCell();
                    dgvCell.Value = user.Prenom+" "+user.Nom;
                    dgvRow.Cells.Add(dgvCell);

                    dgvCell = new DataGridViewTextBoxCell();
                    dgvCell.Value = liste.Contenue;
                    dgvRow.Cells.Add(dgvCell);

                    string etatmsg = "";
                    if (liste.Etat == 1) { etatmsg = "Non lu"; } else { etatmsg = "Lu"; }
                    dgvCell = new DataGridViewTextBoxCell();
                    dgvCell.Value = etatmsg;
                    dgvRow.Cells.Add(dgvCell);

                    dgvCell = new DataGridViewTextBoxCell();
                    dgvCell.Value = liste.DateMess;
                    dgvRow.Cells.Add(dgvCell);

                    data.Rows.Add(dgvRow);
                }
            }
            catch (Exception) { Console.WriteLine("ato @erreur"); }
        }
    }
}
