﻿using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TPT.model;

namespace TPT.service
{
    class SujetService
    {
        string url = Util.getUrl() + "sujet/";

        public List<Sujet> getSujetByIdProf(int idProf)
        {
            url += idProf;
            var client = new RestClient(url);
            IRestResponse response = client.Execute(new RestRequest());
            var content = response.Content;
            JArray releases = JArray.Parse(content);
            List<Sujet> liste = releases.ToObject<List<Sujet>>();
            Console.WriteLine("taille des sujets : "+liste.Count());
            return liste;
        }
        public List<Sujet> getAllSujets(int idprof)
        {
            List<Sujet> result = getSujetByIdProf(idprof);
            return result;
        }

        public void insertDonneListeSujet(List<Sujet> listeDonnee, DataGridView data)
        {
            try
            {
                data.Rows.Clear();
                foreach (Sujet liste in listeDonnee)
                {
                    DataGridViewRow dgvRow = new DataGridViewRow();
                    DataGridViewCell dgvCell = default(DataGridViewCell);

                    dgvCell = new DataGridViewTextBoxCell();
                    dgvCell.Value = liste.IdSujet;
                    dgvRow.Cells.Add(dgvCell);

                    dgvCell = new DataGridViewTextBoxCell();
                    dgvCell.Value = liste.Titre;
                    dgvRow.Cells.Add(dgvCell);

                    dgvCell = new DataGridViewTextBoxCell();
                    dgvCell.Value = liste.DateSujet;
                    dgvRow.Cells.Add(dgvCell);
                    
                    data.Rows.Add(dgvRow);
                }
            }
            catch (Exception) { Console.WriteLine("ato @erreur"); }
        }
    }
}
