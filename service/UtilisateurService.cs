﻿using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TPT.model;
using TPT.service.exception;

namespace TPT.service
{
    class UtilisateurService
    {
        public List<Professeurs> getAllProfesseur()
        {
            string url = Util.getUrl() + "prof";
            var client = new RestClient(url);
            List<Eleve> result = new List<Eleve>();
            IRestResponse response = client.Execute(new RestRequest());
            var content = response.Content;
            JArray releases = JArray.Parse(content);
            List<Professeurs> liste = releases.ToObject<List<Professeurs>>();
            return liste;
        } 

        public List<Professeurs> getAllProfs()
        {
            List<Professeurs> result = getAllProfesseur();
            return result;
        }

        public Utilisateur getOneUtilisateur(string id)
        {
            Utilisateur user = new Utilisateur();
            string url = Util.getUrl() + "utilisateur/" + id;
            var client = new RestClient(url);
            IRestResponse response = client.Execute(new RestRequest());
            var content = response.Content;
            JArray releases = JArray.Parse(content);
            List<Utilisateur> liste = releases.ToObject<List<Utilisateur>>();
            return liste[0];
        }

        public List<Utilisateur> getUtilisateursProf()
        {
            Utilisateur user = new Utilisateur();
            string url = Util.getUrl() + "utilisateur/prof";
            var client = new RestClient(url);
            IRestResponse response = client.Execute(new RestRequest());
            var content = response.Content;
            JArray releases = JArray.Parse(content);
            List<Utilisateur> liste = releases.ToObject<List<Utilisateur>>();
            return liste;
        }

        public Utilisateur getUtilisateurById(string idUser)
        {
            Utilisateur user = new Utilisateur();
            string url = Util.getUrl() + "utilisateur/id/" + idUser;
            var client = new RestClient(url);
            IRestResponse response = client.Execute(new RestRequest());
            var content = response.Content;
            JArray releases = JArray.Parse(content);
            List<Utilisateur> liste = releases.ToObject<List<Utilisateur>>();
            return liste[0];
        }

        
        public Utilisateur getUser(string login, string mdp)
        {
            string url = Util.getUrl()+"utilisateur/" + login + "/" + mdp;
            Utilisateur reponse = new Utilisateur();

            var client = new RestClient(url);
            IRestResponse response = client.Execute(new RestRequest());
            var content = response.Content;
            JArray releases = JArray.Parse(content);
            List<Utilisateur> liste = releases.ToObject<List<Utilisateur>>();
            return liste[0];
        }

        public Professeurs getProf(Utilisateur user)
        {
            return ProfesseurService.getOneProf(user);
        }

        public Secretaires getSec(Utilisateur user)
        {
            return SecretaireService.getOneSecretaire(user);
        }
    }
}
