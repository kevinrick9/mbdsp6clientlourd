﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TPT.model;

namespace TPT.service
{
    class UtilsService
    {
        Professeurs professeurs;

        public Professeurs Professeurs
        {
            get { return professeurs; }
            set { professeurs = value; }
        }

        public DateTime getDateActuelle()
        {
            return DateTime.Today;
        }

        public string dateTimeToString(DateTime d)
        {
            return ((d.ToShortDateString()).Replace('/','-'));
        }

        public DateTime stringToDateTime(string date)
        {
            return Convert.ToDateTime(date);
        }

        public string heureActuelle()
        {
            return DateTime.Now.ToString("HH:mm:ss");
        }

        public DateTime stringToDateTimeHeure(string heure)
        {
            return DateTime.ParseExact(heure, "HH:mm:ss", CultureInfo.InvariantCulture);
        }

        public void date()
        {
            DateTime thisDay = DateTime.Today;
            // Display the date in the default (general) format.
            Console.WriteLine(thisDay.ToString()); //    5/3/2012 12:00:00 AM
            Console.WriteLine();
            // Display the date in a variety of formats.
            Console.WriteLine(thisDay.ToString("d")); //    5/3/2012
            Console.WriteLine(thisDay.ToString("D")); //    Thursday, May 03, 2012
            Console.WriteLine(thisDay.ToString("g")); //    5/3/2012 12:00 AM

            DateTime.Now.ToString("HH:mm:ss"); //result 22:11:45
            DateTime.Now.ToString("hh:mm:ss tt"); //result 11:11:45 PM
            DateTime.Now.ToShortDateString(); //30.5.2012

            DateTime.Now.ToLongTimeString();//its for current date
            DateTime.Now.ToLongDateString();//its for current time
            // for datepicker
            //this.dateTimePickerAdv1.Value = DateTime.Now;
            //this.dateTimePickerAdv1.IsNullDate = false;

            // when wanting to get days of week
            var today = DateTime.Now;
            var nextFriday = today.AddDays(DayOfWeek.Friday - today.DayOfWeek).Date;

            // when wanting to get date from string-base date
            string iDate = "05/05/2005";
            DateTime oDate = Convert.ToDateTime(iDate);
            //MessageBox.Show(oDate.Day + " " + oDate.Month + "  " + oDate.Year);
            string iDate2 = "2005-05-05";
            DateTime oDate2 = DateTime.Parse(iDate2);

            //when wanting to get hour fromstring-based hour
            //String strDate = "24/01/2013 00:00:00";
            String strDate = "00:00:00";
            DateTime date = DateTime.ParseExact(strDate, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
            Console.WriteLine(date);
            //Console.WriteLine(date.ToString("dd/MM/yyyy HH:mm:ss"));
            Console.WriteLine(date.ToString("HH:mm:ss"));
        }
    }
}
