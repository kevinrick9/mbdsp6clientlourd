﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using RestSharp;
using TPT.model;

namespace TPT.service
{
    class SecretaireService
    {
        public static Secretaires getOneSecretaire(Utilisateur user)
        {
            string url = Util.getUrl() + "secretaire/" + user.Id;
            var client = new RestClient(url);
            IRestResponse response = client.Execute(new RestRequest());
            var content = response.Content;
            JArray releases = JArray.Parse(content);
            List<Secretaires> liste = releases.ToObject<List<Secretaires>>();
            //Console.WriteLine("ato professeurservice fonction getoneprof\nla photo : "+liste[0].Photo);
            return liste[0];
        }
    }
}
