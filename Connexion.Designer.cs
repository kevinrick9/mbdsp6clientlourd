﻿namespace TPT
{
    partial class connexion
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelPrincipal = new System.Windows.Forms.Panel();
            this.message = new System.Windows.Forms.Label();
            this.annulerButton = new System.Windows.Forms.Button();
            this.validerButton = new System.Windows.Forms.Button();
            this.mdp = new System.Windows.Forms.TextBox();
            this.login = new System.Windows.Forms.TextBox();
            this.titre2 = new System.Windows.Forms.Label();
            this.titre = new System.Windows.Forms.Label();
            this.panelPrincipal.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelPrincipal
            // 
            this.panelPrincipal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelPrincipal.Controls.Add(this.message);
            this.panelPrincipal.Controls.Add(this.annulerButton);
            this.panelPrincipal.Controls.Add(this.validerButton);
            this.panelPrincipal.Controls.Add(this.mdp);
            this.panelPrincipal.Controls.Add(this.login);
            this.panelPrincipal.Controls.Add(this.titre2);
            this.panelPrincipal.Controls.Add(this.titre);
            this.panelPrincipal.Location = new System.Drawing.Point(4, 5);
            this.panelPrincipal.Name = "panelPrincipal";
            this.panelPrincipal.Size = new System.Drawing.Size(776, 390);
            this.panelPrincipal.TabIndex = 0;
            // 
            // message
            // 
            this.message.AutoSize = true;
            this.message.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.message.ForeColor = System.Drawing.Color.Red;
            this.message.Location = new System.Drawing.Point(263, 110);
            this.message.Name = "message";
            this.message.Size = new System.Drawing.Size(34, 17);
            this.message.TabIndex = 6;
            this.message.Text = "msg";
            this.message.Visible = false;
            // 
            // annulerButton
            // 
            this.annulerButton.BackColor = System.Drawing.Color.Red;
            this.annulerButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.annulerButton.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.annulerButton.Location = new System.Drawing.Point(471, 308);
            this.annulerButton.Name = "annulerButton";
            this.annulerButton.Size = new System.Drawing.Size(117, 44);
            this.annulerButton.TabIndex = 5;
            this.annulerButton.Text = "Annuler";
            this.annulerButton.UseVisualStyleBackColor = false;
            this.annulerButton.Click += new System.EventHandler(this.annulerButton_Click);
            // 
            // validerButton
            // 
            this.validerButton.BackColor = System.Drawing.Color.LightSeaGreen;
            this.validerButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.validerButton.ForeColor = System.Drawing.SystemColors.Control;
            this.validerButton.Location = new System.Drawing.Point(220, 308);
            this.validerButton.Name = "validerButton";
            this.validerButton.Size = new System.Drawing.Size(107, 44);
            this.validerButton.TabIndex = 4;
            this.validerButton.Text = "Valider";
            this.validerButton.UseVisualStyleBackColor = false;
            this.validerButton.Click += new System.EventHandler(this.validerButton_Click);
            // 
            // mdp
            // 
            this.mdp.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mdp.Location = new System.Drawing.Point(266, 227);
            this.mdp.Name = "mdp";
            this.mdp.Size = new System.Drawing.Size(235, 30);
            this.mdp.TabIndex = 3;
            this.mdp.Text = "Mot de passe";
            this.mdp.Click += new System.EventHandler(this.mdp_Click);
            this.mdp.KeyDown += new System.Windows.Forms.KeyEventHandler(this.mdp_KeyDown);
            // 
            // login
            // 
            this.login.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.login.Location = new System.Drawing.Point(266, 146);
            this.login.Name = "login";
            this.login.Size = new System.Drawing.Size(235, 30);
            this.login.TabIndex = 2;
            this.login.Text = "Login";
            this.login.Click += new System.EventHandler(this.login_Click);
            this.login.KeyDown += new System.Windows.Forms.KeyEventHandler(this.login_KeyDown);
            // 
            // titre2
            // 
            this.titre2.AutoSize = true;
            this.titre2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.titre2.Location = new System.Drawing.Point(191, 80);
            this.titre2.Name = "titre2";
            this.titre2.Size = new System.Drawing.Size(397, 17);
            this.titre2.TabIndex = 1;
            this.titre2.Text = "Veuillez entrer vos informations de connexions pour continuer";
            // 
            // titre
            // 
            this.titre.AutoSize = true;
            this.titre.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.titre.Location = new System.Drawing.Point(307, 33);
            this.titre.Name = "titre";
            this.titre.Size = new System.Drawing.Size(143, 31);
            this.titre.TabIndex = 0;
            this.titre.Text = "Connexion";
            // 
            // connexion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 401);
            this.Controls.Add(this.panelPrincipal);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "connexion";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Connexion";
            this.panelPrincipal.ResumeLayout(false);
            this.panelPrincipal.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelPrincipal;
        private System.Windows.Forms.TextBox mdp;
        private System.Windows.Forms.TextBox login;
        private System.Windows.Forms.Label titre2;
        private System.Windows.Forms.Label titre;
        private System.Windows.Forms.Button annulerButton;
        private System.Windows.Forms.Button validerButton;
        private System.Windows.Forms.Label message;
    }
}

