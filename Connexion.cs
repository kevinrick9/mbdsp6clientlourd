﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TPT.controller;

namespace TPT
{
    public partial class connexion : Form
    {
        public connexion()
        {
            InitializeComponent();
        }
        
        public Label Message
        {
            get { return this.message; }
            set { message = value; }
        }

        private void mdp_Click(object sender, EventArgs e)
        {
            mdp.Text = "";
            mdp.UseSystemPasswordChar = true;
        }

        private void login_Click(object sender, EventArgs e)
        {
            login.Text = "";
        }

        private void annulerButton_Click(object sender, EventArgs e)
        {
            message.Visible = true;
            message.Text = "Opération annulée";
            login.Text = "";
            mdp.Text = "";
        }

        private void validerButton_Click(object sender, EventArgs e)
        {
            string login = this.login.Text;
            string mdp = this.mdp.Text;
            AuthentificationController auth = new AuthentificationController();
            auth.authentification(login, mdp, this);
        }

        private void mdp_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                validerButton_Click(sender, e);
            }
        }

        private void login_KeyDown(object sender, KeyEventArgs e)
        {
            if (login.Focused)
            {
                mdp.Text = "";
                mdp.UseSystemPasswordChar = true;
            }
        }

        public connexion getConnection()
        {
            return this;
        }
        /*
        protected override bool ProcessCmdKey(ref Messages msg, Keys keyData)
        {
            bool baseResult = base.ProcessCmdKey(ref msg, keyData);

            if (keyData == Keys.Tab && login.Focused)
            {
                Console.WriteLine("Tab pressed and password focused");
                return true;
            }

            if (keyData == (Keys.Tab | Keys.Shift) && login.Focused)
            {
                Console.WriteLine("Shift-Tab pressed and password focused");
                return true;
            }

            return baseResult;
        } */
    }
}
