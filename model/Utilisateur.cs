﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPT.model
{
    public partial class Utilisateur
    {
        private int id;
        private string nom;
        private string prenom;
        private string mail;
        private string adresse;
        private string login;
        private string mdp;
        private int niveau; // 1:professeur, 2: secretaire
        private int etat;

        public Utilisateur() { }
        public Utilisateur(int id, string nom, string prenom, string mail, string adresse, string login, string mdp, int niveau)
        {
            this.Id = id;
            this.Nom = nom;
            this.Prenom = prenom;
            this.Mail = mail;
            this.Adresse = adresse;
            this.Login = login;
            this.Mdp = mdp;
            this.Niveau = niveau;
        }

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Adresse
        {
            get { return adresse; }
            set { adresse = value; }
        }

        public string Login
        {
            get { return login; }
            set { login = value; }
        }

        public string Mdp
        {
            get { return mdp; }
            set { mdp = value; }
        }

        public string Nom
        {
            get { return nom; }
            set { nom = value; }
        }

        public string Prenom
        {
            get { return prenom; }
            set { prenom = value; }
        }

        public string Mail
        {
            get { return mail; }
            set { mail = value; }
        }

        public int Niveau
        {
            get { return niveau; }
            set { niveau = value; }
        }
    }
}
