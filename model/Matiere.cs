﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPT.model
{
    class Matiere
    {
        private int idMatiere;
        private string nomMatiere;
        private int creditMatiere;
        private int idProf;

        public Matiere() { }
        public Matiere(int id, string nom, int credit)
        {
            this.IdMatiere = id;
            this.NomMatiere = nom;
            this.CreditMatiere = credit;
        }

        public Matiere(int id, string nom, int credit, int idProfesseur)
        {
            this.IdMatiere = id;
            this.NomMatiere = nom;
            this.CreditMatiere = credit;
            this.IdProf = idProfesseur;
        }

        public int IdMatiere
        {
            get { return idMatiere; }
            set { idMatiere = value; }
        }

        public string NomMatiere
        {
            get { return nomMatiere; }
            set { nomMatiere = value; }
        }

        public int CreditMatiere
        {
            get { return creditMatiere; }
            set { creditMatiere = value; }
        }

        public int IdProf
        {
            get { return idProf; }
            set { idProf = value; }
        }
    }
}
