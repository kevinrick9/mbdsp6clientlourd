﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPT.model
{
    class Classe
    {
        int idClasse;
        string nomClasse;
        string anneeScolaire;

        public int IdClasse
        {
            get { return idClasse; }
            set { idClasse = value; }
        }

        public string NomClasse
        {
            get { return nomClasse; }
            set { nomClasse = value; }
        }

        public string AnneeScolaire
        {
            get { return anneeScolaire; }
            set { anneeScolaire = value; }
        }

        public Classe() { }
        public Classe(int id, string nom, string annee)
        {
            this.IdClasse = id;
            this.NomClasse = nom;
            this.AnneeScolaire = annee;
        }
    }
}
