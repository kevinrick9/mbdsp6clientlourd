﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPT.model
{
    class Notification
    {
        private int idNotif;
        private int idSender;
        private int idReceiver;
        private string libelle;
        private string dateNotif;

        public Notification() { }

        public Notification(int id, int idSend, int idReceivr, string libelle, string date)
        {
            this.idNotif = id;
            this.IdSender = idSend;
            this.IdReceiver = idReceivr;
            this.Libelle = libelle;
            this.DateNotif = date;
        }

        public int IdNotif
        {
            get { return idNotif; }
            set { idNotif = value; }
        }

        public int IdSender
        {
            get { return idSender; }
            set { idSender = value; }
        }

        public int IdReceiver
        {
            get { return idReceiver; }
            set { idReceiver = value; }
        }

        public string Libelle
        {
            get { return libelle; }
            set { libelle = value; }
        }

        public string DateNotif
        {
            get { return dateNotif; }
            set { dateNotif = value; }
        }
    }
}
