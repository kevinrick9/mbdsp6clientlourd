﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPT.model
{
    class Cours
    {
        int idCours;
        int idFilieres;
        int idMatiere;
        int idProfs;
        string titreCours;
        string urlCours;
        string dateUpload;
        string heureUpload;
        string nomCours;
        int typeCours;

        public Cours() { }

        public Cours(int id, int idFiliR, int idMatiR, int idProf, string titreCours, string url, 
            string dateUp, string heureUp, string nomCours, int typeCours)
        {
            this.IdCours = id;
            this.IdFilieres = idFiliR;
            this.IdMatieres = idMatiR;
            this.IdProfs = idProf;
            this.TitreCours = titreCours;
            this.UrlCours = url;
            this.DateUpload = dateUp;
            this.HeureUpload = heureUp;
            this.NomCours = nomCours;
            this.TypeCours = typeCours;
        }

        public int IdCours
        {
            get { return idCours; }
            set { idCours = value; }
        }

        public int IdFilieres
        {
            get { return idFilieres; }
            set { idFilieres = value; }
        }

        public int IdMatieres
        {
            get { return idMatiere; }
            set { idMatiere = value; }
        }

        public int IdProfs
        {
            get { return idProfs; }
            set { idProfs = value; }
        }

        public string UrlCours
        {
            get { return urlCours; }
            set { urlCours = value; }
        }

        public string DateUpload
        {
            get { return dateUpload; }
            set { dateUpload = value; }
        }

        public string HeureUpload
        {
            get { return heureUpload; }
            set { heureUpload = value; }
        }

        public string TitreCours
        {
            get { return titreCours; }
            set { titreCours = value; }
        }

        public string NomCours
        {
            get { return nomCours; }
            set { nomCours = value; }
        }

        public int TypeCours
        {
            get { return typeCours; }
            set { typeCours = value; }
        }
    }
}
