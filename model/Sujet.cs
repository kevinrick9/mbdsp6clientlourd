﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPT.model
{
    class Sujet
    {
        int idSujet;
        int idProf;
        string titre;
        string dateSujet;
        string correction;
        int idQcm;

        public Sujet() { }
        public Sujet(int id, int idprof, string titreSujet, string date, string correction, int idqcm)
        {
            this.IdSujet = id;
            this.IdProf = idprof;
            this.Titre = titreSujet;
            this.DateSujet = date;
            this.Correction = correction;
            this.IdQcm = idqcm;
        }

        public int IdSujet
        {
            get { return idSujet; }
            set { idSujet = value; }
        }

        public int IdProf
        {
            get { return idProf; }
            set { idProf = value; }
        }

        public string Titre
        {
            get { return titre; }
            set { titre = value; }
        }

        public string DateSujet
        {
            get { return dateSujet; }
            set { dateSujet = value; }
        }

        public string Correction
        {
            get { return correction; }
            set { correction = value; }
        }

        public int IdQcm
        {
            get { return idQcm; }
            set { idQcm = value; }
        }

    }
}
