﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPT.model
{
    class Filiere
    {
        private int idFiliere;
        private string nom;
        private int credit;
        private string code;

        public Filiere() { }
        public Filiere(int id, string nom, int credit, string code)
        {
            this.IdFiliere = id;
            this.Nom = nom;
            this.Credit = credit;
            this.Code = code;
        }
        public int IdFiliere
        {
            get { return idFiliere; }
            set { idFiliere = value; }
        }

        public string Nom
        {
            get { return nom; }
            set { nom = value; }
        }

        public int Credit
        {
            get { return credit; }
            set { credit = value; }
        }

        public string Code
        {
            get { return code; }
            set { code = value; }
        }
    }
}
