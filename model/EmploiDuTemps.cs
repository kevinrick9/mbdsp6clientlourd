﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPT.model
{
    class EmploiDuTemps
    {
        int idEmploi;
        int idMatiere;
        string dateMatiere;
        string heureDebut;
        string heureFin;
        int jour;
        int codeUser;
        int idCodeCer;

        public EmploiDuTemps() { }
        public EmploiDuTemps(int id, int idMat, string dateMat, string heureDebut, string heureFin, int jour, int codeUser, int idCodeCer){
            this.IdEmploi = id;
            this.IdMatiere = idMat;
            this.DateMatiere = dateMat;
            this.heureDebut = heureDebut;
            this.HeureFin = heureFin;
            this.Jour = jour;
            this.CodeUser = codeUser;
            this.IdCodeCer = idCodeCer;
        }
        public int IdEmploi
        {
            get { return idEmploi; }
            set { idEmploi = value; }
        }

        public string DateMatiere
        {
            get { return dateMatiere; }
            set { dateMatiere = value; }
        }

        public int IdMatiere
        {
            get { return idMatiere; }
            set { idMatiere = value; }
        }

        public string HeureDebut
        {
            get { return heureDebut; }
            set { heureDebut = value; }
        }

        public string HeureFin
        {
            get { return heureFin; }
            set { heureFin = value; }
        }

        public int Jour
        {
            get { return jour; }
            set { jour = value; }
        }

        public int CodeUser
        {
            get { return codeUser; }
            set { codeUser = value; }
        }

        public int IdCodeCer
        {
            get { return idCodeCer; }
            set { idCodeCer = value; }
        }
    }
}
