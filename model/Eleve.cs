﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPT.model
{
    class Eleve 
    {
        int idEtudiants;
        int idClasse;
        string telephone;
        private string nom;

        public string Nom
        {
            get { return nom; }
            set { nom = value; }
        }
        private string prenom;

        public string Prenom
        {
            get { return prenom; }
            set { prenom = value; }
        }
        private string adresse;

        public string Adresse
        {
            get { return adresse; }
            set { adresse = value; }
        }
        private string photo;
        private string email;

        public string Email
        {
            get { return email; }
            set { email = value; }
        }
        private string pseudo;

        public string Pseudo
        {
            get { return pseudo; }
            set { pseudo = value; }
        }
        private string mdp;

        public string Mdp
        {
            get { return mdp; }
            set { mdp = value; }
        }
        int idfilir;
        int codevalide;
        int idClass;
        int ideleves;


        public int IdEtudiants
        {
            get { return idEtudiants; }
            set { idEtudiants = value; }
        }

        public int IdClasse
        {
            get { return idClasse; }
            set { idClasse = value; }
        }

        public string Telephone
        {
            get { return telephone; }
            set { telephone = value; }
        }

        public int Idfilir
        {
            get { return idfilir; }
            set { idfilir = value; }
        }

        public int IdClass
        {
            get { return idClass; }
            set { idClass = value; }
        }

        public int Codevalide
        {
            get { return codevalide; }
            set { codevalide = value; }
        }

        public int Ideleves
        {
            get { return ideleves; }
            set { ideleves = value; }
        }

        public Eleve() { }

        public Eleve(int idEtudiants, int idClasse, string nom, string prenom, string adresse, string telephone,
            string email, string pseudo, string mdp, int idfilir, int codevalide, int idClass, int ideleves)
        {
            this.IdEtudiants = idEtudiants;
            this.IdClasse = idClasse;
            this.Nom = nom;
            this.Prenom = prenom;
            this.Adresse = adresse;
            this.Telephone = telephone;
            this.Email = email;
            this.Pseudo = pseudo;
            this.Mdp = mdp;
            this.Idfilir = idfilir;
            this.Codevalide = codevalide;
            this.IdClass = idClass;
            this.Ideleves = ideleves;
        }
        /*public Eleve(int id, int idUser, int idClasse, int idFiliR)
        {
            //this.Id = idUser;
            this.IdEtudiants = id;
            this.IdClasse = idClasse;
            this.Idfilir = idFiliR;
        } */
    }
}
