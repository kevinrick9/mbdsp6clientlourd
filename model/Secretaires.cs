﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPT.model
{
    public partial class Secretaires : Utilisateur
    {
        private int idSecretR;
        private int idUtilisateur;
        private string photo;
        private string phone;
        private int idDemande;

        public int IdSecretR
        {
            get { return idSecretR; }
            set { idSecretR = value; }
        }

        public string Photo
        {
            get { return photo; }
            set { photo = value; }
        }

        public int IdDemande
        {
            get { return idDemande; }
            set { idDemande = value; }
        }

        public int IdUtilisateur
        {
            get { return idUtilisateur; }
            set { idUtilisateur = value; }
        }

        public string Phone
        {
            get { return phone; }
            set { phone = value; }
        }

        public Secretaires() { }
        public Secretaires(int idSecrtr, int idUser, string idPhotos, int idDmd)
        {
            this.IdSecretR = idSecrtr;
            this.Photo = idPhotos;
            this.IdDemande = idDmd;
            this.IdUtilisateur = idUser;
        }

        public Secretaires(int idSecrtr, string photos, string phones)
        {
            this.IdSecretR = idSecrtr;
            this.Photo = photos;
            this.Phone = phones;
        }
    }
}
