﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPT.model.liaison_classe
{
    class Liaison_Prof_Classe
    {
        private int idProf;

        public int IdProf
        {
            get { return idProf; }
            set { idProf = value; }
        }
        private int idClasse;

        public int IdClasse
        {
            get { return idClasse; }
            set { idClasse = value; }
        }
        public Liaison_Prof_Classe(int idProfs, int idClasses)
        {
            this.IdProf = idProfs;
            this.IdClasse = idClasses;
        }
    }
}
