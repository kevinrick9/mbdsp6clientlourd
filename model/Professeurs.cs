﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPT.model
{
    public partial class Professeurs : Utilisateur
    {
        private int idProf;
        private string photo;
        private int idMatiere;
        private string phone;

        public int IdProf
        {
            get { return idProf; }
            set { idProf = value; }
        }

        public string Phone
        {
            get { return phone; }
            set { phone = value; }
        }

        public string Photo
        {
            get { return photo; }
            set { photo = value; }
        }

        public int IdMatiere
        {
            get { return idMatiere; }
            set { idMatiere = value; }
        }

        public Professeurs() { }
        
        public Professeurs(int idprof, string photo, string phone ) {
            this.IdProf = idProf;
            this.Phone = phone;
            this.Photo = photo;
        } 

        public Professeurs(int idProf,int idUser, string photo, string phone, int idMat)
        {
            this.Id = idUser;
            this.IdProf = idProf;
            this.Phone = phone;
            this.Photo = photo;
            this.IdMatiere = idMat;
        }

        public Professeurs(int idProf, int idUser, string phone, int idMat)
        {
            this.Id = idUser;
            this.IdProf = idProf;
            this.Phone = phone;
            this.Photo = photo;
            this.IdMatiere = idMat;
        }
    }
}
