﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPT.model
{
    class Messages
    {
        private int idMessage;
        private string contenue;
        private int idSource;
        private int idDest;
        private int etat;
        private string dateMess;
        private string heureMess;

        public Messages() { }
        public Messages(int id, string contenu, int idSourc, int idDesti, int etat, string date, string heure)
        {
            this.idMessage = id;
            this.Contenue = contenu;
            this.IdSource = idSourc;
            this.IdDest = idDesti;
            this.Etat = etat;
            this.DateMess = date;
            this.HeureMess = heure;
        }

        public int IdMessage
        {
            get { return idMessage; }
            set { idMessage = value; }
        }

        public string Contenue
        {
            get { return contenue; }
            set { contenue = value; }
        }

        public int IdSource
        {
            get { return idSource; }
            set { idSource = value; }
        }

        public int IdDest
        {
            get { return idDest; }
            set { idDest = value; }
        }

        public int Etat
        {
            get { return etat; }
            set { etat = value; }
        }

        public string DateMess
        {
            get { return dateMess; }
            set { dateMess = value; }
        }

        public string HeureMess
        {
            get { return heureMess; }
            set { heureMess = value; }
        }
    }
}
