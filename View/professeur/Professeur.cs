﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TPT.controller;
using TPT.model;

namespace TPT
{
    public partial class Professeur : Form
    {
        ListeETUController listeCtrller;
        CoursController coursController;
        SujetController sujetController;
        ProfesseurController profCtrl;
        EmploiDuTempsController emploiDuTempsCtrl;
        MessageController msgCtrl;

        private Professeurs professeurs;
        private Utilisateur user;
        private connexion conx;

        public Professeur()
        {
            InitializeComponent();
        }

        public Professeur(Professeurs prof, Utilisateur usr, connexion connx)
        {
            InitializeComponent();
            this.professeurs = prof;
            this.user = usr;
            this.conx = connx;
            this.listeCtrller = new ListeETUController();
            this.coursController = new CoursController();
            this.sujetController = new SujetController();
            this.profCtrl=new ProfesseurController(this);
            this.emploiDuTempsCtrl = new EmploiDuTempsController(this);
            msgCtrl = new MessageController();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.nomUser.Text = "Bienvenu.e "+this.user.Nom+" "+this.user.Prenom;
            this.emploiDuTempsCtrl.getAllEmploiByIdUser();
            this.conx.Hide();
            this.profCtrl.afficherImgProfil();
        }
        private void emploiDuTps_Click(object sender, EventArgs e)
        {
            this.tabPage1.Show();
            this.tabPrincipal.SelectedIndex = 0;
            this.emploiDuTempsCtrl.getAllEmploiByIdUser();
        }

        private void listeEtudiants_Click(object sender, EventArgs e)
        {
            this.tabPage2.Show();
            this.tabPrincipal.SelectedIndex = 1;
            this.listeCtrller.getAllClass(this);
        }

        private void listeCours_Click(object sender, EventArgs e)
        {
            this.tabPage3.Show();
            this.tabPrincipal.SelectedIndex = 2;
            this.coursController.getAllCours(this);
        }

        private void sujetExamen_Click(object sender, EventArgs e)
        {
            this.tabPage4.Show();
            this.tabPrincipal.SelectedIndex = 3;
            this.sujetController.getAllSujet(this);
        }

        private void message_Click(object sender, EventArgs e)
        {
            this.tabPage5.Show();
            this.tabPrincipal.SelectedIndex = 4;
            this.msgCtrl.insertionDonneeMsg(this);
        }
        
        private void label1_Click(object sender, EventArgs e)
        {

        }

        public ComboBox getListeDeroulanteClasse()
        {
            return this.listeClasse;
        }
        public Label getNomClasseLabel()
        {
            return this.nomClasse;
        }
        public Label getNbreEtu()
        {
            return this.nbreETU;
        }
        public DataGridView getTableauListeETU()
        {
            return this.dataGridViewListeETU;
        }

        private void listeClasse_SelectedIndexChanged(object sender, EventArgs e)
        {
            string value = ((KeyValuePair<int, string>)listeClasse.SelectedItem).Value;
            int key = ((KeyValuePair<int, string>)listeClasse.SelectedItem).Key;
            this.listeCtrller.idClasse = key;
            this.listeCtrller.getAllClasses2(this);
            this.nomClasse.Text = value;
            //Console.WriteLine("niova : " + value);
        }

        private void tabPrincipal_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (this.tabPage1.Focus())
            {
                this.emploiDuTempsCtrl.getAllEmploiByIdUser();
            }

            if (this.tabPage2.Focus())
            {
                this.listeCtrller.getAllClass(this);
            }

            if (this.tabPage3.Focus())
            {
                this.coursController.getAllCours(this);
            }

            if (this.tabPage4.Focus())
            {
                this.sujetController.getAllSujet(this);
            }

            if (this.tabPage5.Focus())
            {
                this.msgCtrl.insertionDonneeMsg(this);
            }

            if (this.tabPage6.Focus())
            {
                fonctionImage();
            }
        }

        private void fonctionImage(){
            this.profCtrl.affichageProfilUtilisateur();
            this.profCtrl.afficherImageUser();
            this.labelTitreImg.Text = "Image de  profil de "+this.user.Prenom+" "+this.user.Nom;
        }
        public Professeurs getProfesseurs()
        {
            return this.professeurs;
        }
        public Utilisateur getUtilisateur()
        {
            return this.user;
        }

        public Label getLabelNomUser()
        {
            return this.nomUser;
        }

        public List<Label> getListeLabelProfil()
        {
            List<Label> result = new List<Label>();
            result.Add(labelTitreProfil);
            result.Add(valeurNom);          result.Add(valeurPrenom);
            result.Add(valeurAdresse);      result.Add(valeurEmail);
            result.Add(valeurPhone);        result.Add(valeurPseudo);
            result.Add(valeurMdp);
            return result;
        }

        public PictureBox getImgUser()
        {
            return this.imgProfil;
        }
        public PictureBox getImgUser2()
        {
            return this.imageUser;
        }

        public DataGridView getTableauEmploiDuTemps()
        {
            return this.tableauEmploiDuTemps;
        }

        public Label getTitreEmploiDuTempsProfs()
        {
            return this.titreEmploiDutemps;
        }
        public Label getNbreMsgGot()
        {
            return this.nbreMsg;
        }

        public DataGridView getTableauListeCours()
        {
            return this.tableauListeDCours;
        }
        public DataGridView getTableauListeSujet()
        {
            return this.dataGridViewListeSujet;
        }
        public DataGridView getTableauListeMsg()
        {
            return this.dataGridViewListeMsg;
        }

        private void imageUser_Click(object sender, EventArgs e)
        {
            fonctionImage();
            this.tabPage6.Show();
            this.tabPrincipal.SelectedIndex = 5;
        }

        public ComboBox getListeDeroulanteStatutMsg()
        {
            return this.statutMsg;
        }


        private void Professeur_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.conx.Close();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.emploiDuTempsCtrl.getAllEmploiByIdUser();
            this.profCtrl.afficherImgProfil();
            this.emploiDuTempsCtrl.getAllEmploiByIdUser();
            this.listeCtrller.getAllClass(this);
            this.coursController.getAllCours(this);
            this.sujetController.getAllSujet(this);
            this.msgCtrl.insertionDonneeMsg(this);
        }

    }
}
