﻿namespace TPT
{
    partial class Professeur
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Professeur));
            this.panelContenu = new System.Windows.Forms.Panel();
            this.tabPrincipal = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.titreEmploiDutemps = new System.Windows.Forms.Label();
            this.tableauEmploiDuTemps = new System.Windows.Forms.DataGridView();
            this.Jour = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.heureDebut = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.heureFin = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nomMatiere = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.nbreETU = new System.Windows.Forms.Label();
            this.labelNbreETU = new System.Windows.Forms.Label();
            this.nomClasse = new System.Windows.Forms.Label();
            this.listeClasse = new System.Windows.Forms.ComboBox();
            this.dataGridViewListeETU = new System.Windows.Forms.DataGridView();
            this.Numero = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Prenom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.titreListeCours = new System.Windows.Forms.Label();
            this.tableauListeDCours = new System.Windows.Forms.DataGridView();
            this.nomCours = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.url = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DateUpload = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.typeCours = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridViewListeSujet = new System.Windows.Forms.DataGridView();
            this.Identifiant = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.nbreMsg = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.dataGridViewListeMsg = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DateMess = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statutMsg = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.labelTitreImg = new System.Windows.Forms.Label();
            this.valeurPseudo = new System.Windows.Forms.Label();
            this.valeurPhone = new System.Windows.Forms.Label();
            this.valeurEmail = new System.Windows.Forms.Label();
            this.valeurAdresse = new System.Windows.Forms.Label();
            this.valeurPrenom = new System.Windows.Forms.Label();
            this.valeurNom = new System.Windows.Forms.Label();
            this.labelMdp = new System.Windows.Forms.Label();
            this.labelPseudo = new System.Windows.Forms.Label();
            this.labelPhone = new System.Windows.Forms.Label();
            this.labelMail = new System.Windows.Forms.Label();
            this.labelAdresse = new System.Windows.Forms.Label();
            this.labelPrenom = new System.Windows.Forms.Label();
            this.labelNom = new System.Windows.Forms.Label();
            this.labelTitreProfil = new System.Windows.Forms.Label();
            this.imgProfil = new System.Windows.Forms.PictureBox();
            this.valeurMdp = new System.Windows.Forms.Label();
            this.panelMenu = new System.Windows.Forms.Panel();
            this.nomUser = new System.Windows.Forms.Label();
            this.imageUser = new System.Windows.Forms.PictureBox();
            this.message = new System.Windows.Forms.Label();
            this.sujetExamen = new System.Windows.Forms.Label();
            this.listeCours = new System.Windows.Forms.Label();
            this.listeEtudiants = new System.Windows.Forms.Label();
            this.emploiDuTps = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panelContenu.SuspendLayout();
            this.tabPrincipal.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tableauEmploiDuTemps)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewListeETU)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tableauListeDCours)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewListeSujet)).BeginInit();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewListeMsg)).BeginInit();
            this.tabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgProfil)).BeginInit();
            this.panelMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageUser)).BeginInit();
            this.SuspendLayout();
            // 
            // panelContenu
            // 
            this.panelContenu.Controls.Add(this.tabPrincipal);
            this.panelContenu.Location = new System.Drawing.Point(178, 3);
            this.panelContenu.Name = "panelContenu";
            this.panelContenu.Size = new System.Drawing.Size(1045, 484);
            this.panelContenu.TabIndex = 0;
            // 
            // tabPrincipal
            // 
            this.tabPrincipal.Controls.Add(this.tabPage1);
            this.tabPrincipal.Controls.Add(this.tabPage2);
            this.tabPrincipal.Controls.Add(this.tabPage3);
            this.tabPrincipal.Controls.Add(this.tabPage4);
            this.tabPrincipal.Controls.Add(this.tabPage5);
            this.tabPrincipal.Controls.Add(this.tabPage6);
            this.tabPrincipal.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPrincipal.ItemSize = new System.Drawing.Size(90, 30);
            this.tabPrincipal.Location = new System.Drawing.Point(3, 3);
            this.tabPrincipal.Name = "tabPrincipal";
            this.tabPrincipal.SelectedIndex = 0;
            this.tabPrincipal.Size = new System.Drawing.Size(1039, 478);
            this.tabPrincipal.TabIndex = 2;
            this.tabPrincipal.SelectedIndexChanged += new System.EventHandler(this.tabPrincipal_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.titreEmploiDutemps);
            this.tabPage1.Controls.Add(this.tableauEmploiDuTemps);
            this.tabPage1.Location = new System.Drawing.Point(4, 34);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1031, 440);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Emploi du temps";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // titreEmploiDutemps
            // 
            this.titreEmploiDutemps.AutoSize = true;
            this.titreEmploiDutemps.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.titreEmploiDutemps.Location = new System.Drawing.Point(236, 22);
            this.titreEmploiDutemps.Name = "titreEmploiDutemps";
            this.titreEmploiDutemps.Size = new System.Drawing.Size(166, 20);
            this.titreEmploiDutemps.TabIndex = 1;
            this.titreEmploiDutemps.Text = "Mon Emploi du temps ";
            // 
            // tableauEmploiDuTemps
            // 
            this.tableauEmploiDuTemps.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tableauEmploiDuTemps.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Jour,
            this.heureDebut,
            this.heureFin,
            this.nomMatiere});
            this.tableauEmploiDuTemps.Location = new System.Drawing.Point(6, 57);
            this.tableauEmploiDuTemps.Name = "tableauEmploiDuTemps";
            this.tableauEmploiDuTemps.Size = new System.Drawing.Size(1019, 377);
            this.tableauEmploiDuTemps.TabIndex = 0;
            // 
            // Jour
            // 
            this.Jour.HeaderText = "Jour";
            this.Jour.Name = "Jour";
            this.Jour.Width = 200;
            // 
            // heureDebut
            // 
            this.heureDebut.HeaderText = "heure début";
            this.heureDebut.Name = "heureDebut";
            this.heureDebut.Width = 250;
            // 
            // heureFin
            // 
            this.heureFin.HeaderText = "heure fin";
            this.heureFin.Name = "heureFin";
            this.heureFin.Width = 250;
            // 
            // nomMatiere
            // 
            this.nomMatiere.HeaderText = "Matière";
            this.nomMatiere.Name = "nomMatiere";
            this.nomMatiere.Width = 300;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.nbreETU);
            this.tabPage2.Controls.Add(this.labelNbreETU);
            this.tabPage2.Controls.Add(this.nomClasse);
            this.tabPage2.Controls.Add(this.listeClasse);
            this.tabPage2.Controls.Add(this.dataGridViewListeETU);
            this.tabPage2.Location = new System.Drawing.Point(4, 34);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1031, 440);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Liste des étudiants";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // nbreETU
            // 
            this.nbreETU.AutoSize = true;
            this.nbreETU.Location = new System.Drawing.Point(765, 28);
            this.nbreETU.Name = "nbreETU";
            this.nbreETU.Size = new System.Drawing.Size(27, 20);
            this.nbreETU.TabIndex = 4;
            this.nbreETU.Text = "25";
            // 
            // labelNbreETU
            // 
            this.labelNbreETU.AutoSize = true;
            this.labelNbreETU.Location = new System.Drawing.Point(606, 27);
            this.labelNbreETU.Name = "labelNbreETU";
            this.labelNbreETU.Size = new System.Drawing.Size(159, 20);
            this.labelNbreETU.TabIndex = 3;
            this.labelNbreETU.Text = "Nombre d\'étudiants : ";
            // 
            // nomClasse
            // 
            this.nomClasse.AutoSize = true;
            this.nomClasse.Location = new System.Drawing.Point(471, 26);
            this.nomClasse.Name = "nomClasse";
            this.nomClasse.Size = new System.Drawing.Size(70, 20);
            this.nomClasse.TabIndex = 2;
            this.nomClasse.Text = "Classe 1";
            this.nomClasse.Click += new System.EventHandler(this.label1_Click);
            // 
            // listeClasse
            // 
            this.listeClasse.FormattingEnabled = true;
            this.listeClasse.Items.AddRange(new object[] {
            "Classe 1",
            "Classe 2",
            "Classe 3"});
            this.listeClasse.Location = new System.Drawing.Point(93, 21);
            this.listeClasse.Name = "listeClasse";
            this.listeClasse.Size = new System.Drawing.Size(262, 28);
            this.listeClasse.TabIndex = 1;
            this.listeClasse.SelectedIndexChanged += new System.EventHandler(this.listeClasse_SelectedIndexChanged);
            // 
            // dataGridViewListeETU
            // 
            this.dataGridViewListeETU.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewListeETU.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Numero,
            this.Nom,
            this.Prenom});
            this.dataGridViewListeETU.Location = new System.Drawing.Point(6, 76);
            this.dataGridViewListeETU.Name = "dataGridViewListeETU";
            this.dataGridViewListeETU.Size = new System.Drawing.Size(944, 297);
            this.dataGridViewListeETU.TabIndex = 0;
            // 
            // Numero
            // 
            this.Numero.HeaderText = "Numero";
            this.Numero.Name = "Numero";
            // 
            // Nom
            // 
            this.Nom.HeaderText = "Nom";
            this.Nom.Name = "Nom";
            this.Nom.Width = 300;
            // 
            // Prenom
            // 
            this.Prenom.HeaderText = "Prenom";
            this.Prenom.Name = "Prenom";
            this.Prenom.Width = 500;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.titreListeCours);
            this.tabPage3.Controls.Add(this.tableauListeDCours);
            this.tabPage3.Location = new System.Drawing.Point(4, 34);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1031, 440);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Liste des cours";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // titreListeCours
            // 
            this.titreListeCours.AutoSize = true;
            this.titreListeCours.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.titreListeCours.Location = new System.Drawing.Point(334, 31);
            this.titreListeCours.Name = "titreListeCours";
            this.titreListeCours.Size = new System.Drawing.Size(319, 20);
            this.titreListeCours.TabIndex = 4;
            this.titreListeCours.Text = "Liste des cours que vous avez uploadé";
            // 
            // tableauListeDCours
            // 
            this.tableauListeDCours.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tableauListeDCours.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nomCours,
            this.url,
            this.DateUpload,
            this.typeCours});
            this.tableauListeDCours.Location = new System.Drawing.Point(6, 87);
            this.tableauListeDCours.Name = "tableauListeDCours";
            this.tableauListeDCours.Size = new System.Drawing.Size(988, 331);
            this.tableauListeDCours.TabIndex = 2;
            // 
            // nomCours
            // 
            this.nomCours.HeaderText = "Nom";
            this.nomCours.Name = "nomCours";
            this.nomCours.Width = 150;
            // 
            // url
            // 
            this.url.HeaderText = "URL";
            this.url.Name = "url";
            this.url.Width = 500;
            // 
            // DateUpload
            // 
            this.DateUpload.HeaderText = "Date upload";
            this.DateUpload.Name = "DateUpload";
            this.DateUpload.Width = 150;
            // 
            // typeCours
            // 
            this.typeCours.HeaderText = "Type cours";
            this.typeCours.Name = "typeCours";
            this.typeCours.Width = 150;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.label1);
            this.tabPage4.Controls.Add(this.dataGridViewListeSujet);
            this.tabPage4.Location = new System.Drawing.Point(4, 34);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(1031, 440);
            this.tabPage4.TabIndex = 6;
            this.tabPage4.Text = "Liste des sujets";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(349, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(323, 20);
            this.label1.TabIndex = 6;
            this.label1.Text = "Liste des sujets que vous avez uploadé";
            // 
            // dataGridViewListeSujet
            // 
            this.dataGridViewListeSujet.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewListeSujet.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Identifiant,
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2});
            this.dataGridViewListeSujet.Location = new System.Drawing.Point(169, 87);
            this.dataGridViewListeSujet.Name = "dataGridViewListeSujet";
            this.dataGridViewListeSujet.Size = new System.Drawing.Size(692, 331);
            this.dataGridViewListeSujet.TabIndex = 5;
            // 
            // Identifiant
            // 
            this.Identifiant.HeaderText = "Identifiant";
            this.Identifiant.Name = "Identifiant";
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Titre";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 350;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Date";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 200;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.nbreMsg);
            this.tabPage5.Controls.Add(this.label4);
            this.tabPage5.Controls.Add(this.dataGridViewListeMsg);
            this.tabPage5.Controls.Add(this.statutMsg);
            this.tabPage5.Controls.Add(this.label5);
            this.tabPage5.Location = new System.Drawing.Point(4, 34);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(1031, 440);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Messages";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // nbreMsg
            // 
            this.nbreMsg.AutoSize = true;
            this.nbreMsg.Location = new System.Drawing.Point(846, 26);
            this.nbreMsg.Name = "nbreMsg";
            this.nbreMsg.Size = new System.Drawing.Size(27, 20);
            this.nbreMsg.TabIndex = 14;
            this.nbreMsg.Text = "20";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(676, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(176, 20);
            this.label4.TabIndex = 13;
            this.label4.Text = "Nombre de messages : ";
            // 
            // dataGridViewListeMsg
            // 
            this.dataGridViewListeMsg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewListeMsg.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn4,
            this.DateMess});
            this.dataGridViewListeMsg.Location = new System.Drawing.Point(3, 73);
            this.dataGridViewListeMsg.Name = "dataGridViewListeMsg";
            this.dataGridViewListeMsg.Size = new System.Drawing.Size(1028, 331);
            this.dataGridViewListeMsg.TabIndex = 12;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Source";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Width = 200;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "Contenu";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Width = 500;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "Etat";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // DateMess
            // 
            this.DateMess.HeaderText = "Date du message";
            this.DateMess.Name = "DateMess";
            this.DateMess.Width = 250;
            // 
            // statutMsg
            // 
            this.statutMsg.FormattingEnabled = true;
            this.statutMsg.Items.AddRange(new object[] {
            "Classe 1",
            "Classe 2",
            "Classe 3"});
            this.statutMsg.Location = new System.Drawing.Point(274, 23);
            this.statutMsg.Name = "statutMsg";
            this.statutMsg.Size = new System.Drawing.Size(157, 28);
            this.statutMsg.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(517, 26);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 20);
            this.label5.TabIndex = 10;
            this.label5.Text = "Classe 1";
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.labelTitreImg);
            this.tabPage6.Controls.Add(this.valeurPseudo);
            this.tabPage6.Controls.Add(this.valeurPhone);
            this.tabPage6.Controls.Add(this.valeurEmail);
            this.tabPage6.Controls.Add(this.valeurAdresse);
            this.tabPage6.Controls.Add(this.valeurPrenom);
            this.tabPage6.Controls.Add(this.valeurNom);
            this.tabPage6.Controls.Add(this.labelMdp);
            this.tabPage6.Controls.Add(this.labelPseudo);
            this.tabPage6.Controls.Add(this.labelPhone);
            this.tabPage6.Controls.Add(this.labelMail);
            this.tabPage6.Controls.Add(this.labelAdresse);
            this.tabPage6.Controls.Add(this.labelPrenom);
            this.tabPage6.Controls.Add(this.labelNom);
            this.tabPage6.Controls.Add(this.labelTitreProfil);
            this.tabPage6.Controls.Add(this.imgProfil);
            this.tabPage6.Controls.Add(this.valeurMdp);
            this.tabPage6.Location = new System.Drawing.Point(4, 34);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(1031, 440);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "Mon Profil";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // labelTitreImg
            // 
            this.labelTitreImg.AutoSize = true;
            this.labelTitreImg.Location = new System.Drawing.Point(91, 306);
            this.labelTitreImg.Name = "labelTitreImg";
            this.labelTitreImg.Size = new System.Drawing.Size(51, 20);
            this.labelTitreImg.TabIndex = 16;
            this.labelTitreImg.Text = "label6";
            // 
            // valeurPseudo
            // 
            this.valeurPseudo.AutoSize = true;
            this.valeurPseudo.Location = new System.Drawing.Point(583, 295);
            this.valeurPseudo.Name = "valeurPseudo";
            this.valeurPseudo.Size = new System.Drawing.Size(60, 20);
            this.valeurPseudo.TabIndex = 14;
            this.valeurPseudo.Text = "label11";
            // 
            // valeurPhone
            // 
            this.valeurPhone.AutoSize = true;
            this.valeurPhone.Location = new System.Drawing.Point(583, 255);
            this.valeurPhone.Name = "valeurPhone";
            this.valeurPhone.Size = new System.Drawing.Size(60, 20);
            this.valeurPhone.TabIndex = 13;
            this.valeurPhone.Text = "label10";
            // 
            // valeurEmail
            // 
            this.valeurEmail.AutoSize = true;
            this.valeurEmail.Location = new System.Drawing.Point(583, 215);
            this.valeurEmail.Name = "valeurEmail";
            this.valeurEmail.Size = new System.Drawing.Size(51, 20);
            this.valeurEmail.TabIndex = 12;
            this.valeurEmail.Text = "label9";
            // 
            // valeurAdresse
            // 
            this.valeurAdresse.AutoSize = true;
            this.valeurAdresse.Location = new System.Drawing.Point(583, 175);
            this.valeurAdresse.Name = "valeurAdresse";
            this.valeurAdresse.Size = new System.Drawing.Size(51, 20);
            this.valeurAdresse.TabIndex = 11;
            this.valeurAdresse.Text = "label8";
            // 
            // valeurPrenom
            // 
            this.valeurPrenom.AutoSize = true;
            this.valeurPrenom.Location = new System.Drawing.Point(583, 135);
            this.valeurPrenom.Name = "valeurPrenom";
            this.valeurPrenom.Size = new System.Drawing.Size(51, 20);
            this.valeurPrenom.TabIndex = 10;
            this.valeurPrenom.Text = "label7";
            // 
            // valeurNom
            // 
            this.valeurNom.AutoSize = true;
            this.valeurNom.Location = new System.Drawing.Point(583, 95);
            this.valeurNom.Name = "valeurNom";
            this.valeurNom.Size = new System.Drawing.Size(51, 20);
            this.valeurNom.TabIndex = 9;
            this.valeurNom.Text = "label6";
            // 
            // labelMdp
            // 
            this.labelMdp.AutoSize = true;
            this.labelMdp.Location = new System.Drawing.Point(430, 335);
            this.labelMdp.Name = "labelMdp";
            this.labelMdp.Size = new System.Drawing.Size(117, 20);
            this.labelMdp.TabIndex = 8;
            this.labelMdp.Text = "Mot de passe : ";
            // 
            // labelPseudo
            // 
            this.labelPseudo.AutoSize = true;
            this.labelPseudo.Location = new System.Drawing.Point(430, 295);
            this.labelPseudo.Name = "labelPseudo";
            this.labelPseudo.Size = new System.Drawing.Size(75, 20);
            this.labelPseudo.TabIndex = 7;
            this.labelPseudo.Text = "Pseudo : ";
            // 
            // labelPhone
            // 
            this.labelPhone.AutoSize = true;
            this.labelPhone.Location = new System.Drawing.Point(430, 255);
            this.labelPhone.Name = "labelPhone";
            this.labelPhone.Size = new System.Drawing.Size(96, 20);
            this.labelPhone.TabIndex = 6;
            this.labelPhone.Text = "Téléphone : ";
            // 
            // labelMail
            // 
            this.labelMail.AutoSize = true;
            this.labelMail.Location = new System.Drawing.Point(430, 215);
            this.labelMail.Name = "labelMail";
            this.labelMail.Size = new System.Drawing.Size(48, 20);
            this.labelMail.TabIndex = 5;
            this.labelMail.Text = "Email";
            // 
            // labelAdresse
            // 
            this.labelAdresse.AutoSize = true;
            this.labelAdresse.Location = new System.Drawing.Point(430, 175);
            this.labelAdresse.Name = "labelAdresse";
            this.labelAdresse.Size = new System.Drawing.Size(80, 20);
            this.labelAdresse.TabIndex = 4;
            this.labelAdresse.Text = "Adresse : ";
            // 
            // labelPrenom
            // 
            this.labelPrenom.AutoSize = true;
            this.labelPrenom.Location = new System.Drawing.Point(430, 135);
            this.labelPrenom.Name = "labelPrenom";
            this.labelPrenom.Size = new System.Drawing.Size(72, 20);
            this.labelPrenom.TabIndex = 3;
            this.labelPrenom.Text = "Prénom :";
            // 
            // labelNom
            // 
            this.labelNom.AutoSize = true;
            this.labelNom.Location = new System.Drawing.Point(430, 95);
            this.labelNom.Name = "labelNom";
            this.labelNom.Size = new System.Drawing.Size(54, 20);
            this.labelNom.TabIndex = 2;
            this.labelNom.Text = "Nom : ";
            // 
            // labelTitreProfil
            // 
            this.labelTitreProfil.AutoSize = true;
            this.labelTitreProfil.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTitreProfil.Location = new System.Drawing.Point(352, 28);
            this.labelTitreProfil.Name = "labelTitreProfil";
            this.labelTitreProfil.Size = new System.Drawing.Size(66, 24);
            this.labelTitreProfil.TabIndex = 1;
            this.labelTitreProfil.Text = "label6";
            // 
            // imgProfil
            // 
            this.imgProfil.Location = new System.Drawing.Point(83, 105);
            this.imgProfil.Name = "imgProfil";
            this.imgProfil.Size = new System.Drawing.Size(188, 175);
            this.imgProfil.TabIndex = 0;
            this.imgProfil.TabStop = false;
            // 
            // valeurMdp
            // 
            this.valeurMdp.AutoSize = true;
            this.valeurMdp.Font = new System.Drawing.Font("Microsoft Sans Serif", 56F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.valeurMdp.Location = new System.Drawing.Point(563, 282);
            this.valeurMdp.Name = "valeurMdp";
            this.valeurMdp.Size = new System.Drawing.Size(100, 85);
            this.valeurMdp.TabIndex = 15;
            this.valeurMdp.Text = "...";
            // 
            // panelMenu
            // 
            this.panelMenu.Controls.Add(this.nomUser);
            this.panelMenu.Controls.Add(this.imageUser);
            this.panelMenu.Controls.Add(this.message);
            this.panelMenu.Controls.Add(this.sujetExamen);
            this.panelMenu.Controls.Add(this.listeCours);
            this.panelMenu.Controls.Add(this.listeEtudiants);
            this.panelMenu.Controls.Add(this.emploiDuTps);
            this.panelMenu.Location = new System.Drawing.Point(1, 3);
            this.panelMenu.Name = "panelMenu";
            this.panelMenu.Size = new System.Drawing.Size(171, 455);
            this.panelMenu.TabIndex = 0;
            // 
            // nomUser
            // 
            this.nomUser.AutoSize = true;
            this.nomUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nomUser.Location = new System.Drawing.Point(4, 172);
            this.nomUser.Name = "nomUser";
            this.nomUser.Size = new System.Drawing.Size(71, 17);
            this.nomUser.TabIndex = 0;
            this.nomUser.Text = "Nom User";
            this.nomUser.Click += new System.EventHandler(this.imageUser_Click);
            // 
            // imageUser
            // 
            this.imageUser.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imageUser.Image = ((System.Drawing.Image)(resources.GetObject("imageUser.Image")));
            this.imageUser.Location = new System.Drawing.Point(11, 3);
            this.imageUser.Name = "imageUser";
            this.imageUser.Size = new System.Drawing.Size(151, 144);
            this.imageUser.TabIndex = 0;
            this.imageUser.TabStop = false;
            this.imageUser.Click += new System.EventHandler(this.imageUser_Click);
            // 
            // message
            // 
            this.message.AutoSize = true;
            this.message.Cursor = System.Windows.Forms.Cursors.Hand;
            this.message.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.message.Location = new System.Drawing.Point(11, 379);
            this.message.Name = "message";
            this.message.Size = new System.Drawing.Size(82, 20);
            this.message.TabIndex = 4;
            this.message.Text = "Messages";
            this.message.Click += new System.EventHandler(this.message_Click);
            // 
            // sujetExamen
            // 
            this.sujetExamen.AutoSize = true;
            this.sujetExamen.Cursor = System.Windows.Forms.Cursors.Hand;
            this.sujetExamen.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sujetExamen.Location = new System.Drawing.Point(11, 339);
            this.sujetExamen.Name = "sujetExamen";
            this.sujetExamen.Size = new System.Drawing.Size(84, 20);
            this.sujetExamen.TabIndex = 3;
            this.sujetExamen.Text = "Liste Sujet";
            this.sujetExamen.Click += new System.EventHandler(this.sujetExamen_Click);
            // 
            // listeCours
            // 
            this.listeCours.AutoSize = true;
            this.listeCours.Cursor = System.Windows.Forms.Cursors.Hand;
            this.listeCours.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listeCours.Location = new System.Drawing.Point(11, 299);
            this.listeCours.Name = "listeCours";
            this.listeCours.Size = new System.Drawing.Size(116, 20);
            this.listeCours.TabIndex = 2;
            this.listeCours.Text = "Liste des cours";
            this.listeCours.Click += new System.EventHandler(this.listeCours_Click);
            // 
            // listeEtudiants
            // 
            this.listeEtudiants.AutoSize = true;
            this.listeEtudiants.Cursor = System.Windows.Forms.Cursors.Hand;
            this.listeEtudiants.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listeEtudiants.Location = new System.Drawing.Point(11, 259);
            this.listeEtudiants.Name = "listeEtudiants";
            this.listeEtudiants.Size = new System.Drawing.Size(143, 20);
            this.listeEtudiants.TabIndex = 1;
            this.listeEtudiants.Text = "Liste des étudiants";
            this.listeEtudiants.Click += new System.EventHandler(this.listeEtudiants_Click);
            // 
            // emploiDuTps
            // 
            this.emploiDuTps.AutoSize = true;
            this.emploiDuTps.Cursor = System.Windows.Forms.Cursors.Hand;
            this.emploiDuTps.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.emploiDuTps.Location = new System.Drawing.Point(11, 219);
            this.emploiDuTps.Name = "emploiDuTps";
            this.emploiDuTps.Size = new System.Drawing.Size(127, 20);
            this.emploiDuTps.TabIndex = 0;
            this.emploiDuTps.Text = "Emploi du temps";
            this.emploiDuTps.Click += new System.EventHandler(this.emploiDuTps_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 5000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Professeur
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1223, 486);
            this.Controls.Add(this.panelMenu);
            this.Controls.Add(this.panelContenu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "Professeur";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Professeurs";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Professeur_FormClosed);
            this.panelContenu.ResumeLayout(false);
            this.tabPrincipal.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tableauEmploiDuTemps)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewListeETU)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tableauListeDCours)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewListeSujet)).EndInit();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewListeMsg)).EndInit();
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgProfil)).EndInit();
            this.panelMenu.ResumeLayout(false);
            this.panelMenu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageUser)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelContenu;
        private System.Windows.Forms.Panel panelMenu;
        private System.Windows.Forms.Label emploiDuTps;
        private System.Windows.Forms.Label message;
        private System.Windows.Forms.Label sujetExamen;
        private System.Windows.Forms.Label listeCours;
        private System.Windows.Forms.Label listeEtudiants;
        private System.Windows.Forms.Label nomUser;
        private System.Windows.Forms.PictureBox imageUser;
        private System.Windows.Forms.Label titreEmploiDutemps;
        private System.Windows.Forms.DataGridView tableauEmploiDuTemps;
        private System.Windows.Forms.TabControl tabPrincipal;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.ComboBox listeClasse;
        private System.Windows.Forms.DataGridView dataGridViewListeETU;
        private System.Windows.Forms.DataGridViewTextBoxColumn Numero;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nom;
        private System.Windows.Forms.DataGridViewTextBoxColumn Prenom;
        private System.Windows.Forms.Label nomClasse;
        private System.Windows.Forms.Label nbreETU;
        private System.Windows.Forms.Label labelNbreETU;
        private System.Windows.Forms.DataGridView tableauListeDCours;
        private System.Windows.Forms.Label nbreMsg;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView dataGridViewListeMsg;
        private System.Windows.Forms.ComboBox statutMsg;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Jour;
        private System.Windows.Forms.DataGridViewTextBoxColumn heureDebut;
        private System.Windows.Forms.DataGridViewTextBoxColumn heureFin;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomMatiere;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.Label labelMdp;
        private System.Windows.Forms.Label labelPseudo;
        private System.Windows.Forms.Label labelPhone;
        private System.Windows.Forms.Label labelMail;
        private System.Windows.Forms.Label labelAdresse;
        private System.Windows.Forms.Label labelPrenom;
        private System.Windows.Forms.Label labelNom;
        private System.Windows.Forms.Label labelTitreProfil;
        private System.Windows.Forms.PictureBox imgProfil;
        private System.Windows.Forms.Label valeurMdp;
        private System.Windows.Forms.Label valeurPseudo;
        private System.Windows.Forms.Label valeurPhone;
        private System.Windows.Forms.Label valeurEmail;
        private System.Windows.Forms.Label valeurAdresse;
        private System.Windows.Forms.Label valeurPrenom;
        private System.Windows.Forms.Label valeurNom;
        private System.Windows.Forms.Label labelTitreImg;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomCours;
        private System.Windows.Forms.DataGridViewTextBoxColumn url;
        private System.Windows.Forms.DataGridViewTextBoxColumn DateUpload;
        private System.Windows.Forms.DataGridViewTextBoxColumn typeCours;
        private System.Windows.Forms.Label titreListeCours;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridViewListeSujet;
        private System.Windows.Forms.DataGridViewTextBoxColumn Identifiant;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn DateMess;
        private System.Windows.Forms.Timer timer1;
    }
}