﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TPT.controller;
using TPT.model;

namespace TPT
{
    public partial class Secretaire : Form
    {
        protected override void OnLoad(EventArgs e)
        {
            // do stuff before Load-event is raised
            base.OnLoad(e);
            // do stuff after Load-event was raised

            this.listeCtrller.getAllClass(this);
            this.conx.Hide();
            this.secretaireCtrller.afficherImgProfil();
        }

        ListeETUController listeCtrller;
        SecretaireController secretaireCtrller;
        NotificationController notifCtrler;

        private Secretaires secretR;
        private Utilisateur user;
        private connexion conx;
        public Secretaire()
        {
            InitializeComponent();
        }

        public Secretaire(Secretaires sec, Utilisateur usr, connexion connx)
        {
            InitializeComponent();
            this.nbreNotif_en_Haut.BackColor = Color.Transparent;
            this.secretR = sec;
            this.user = usr;
            this.conx = connx;
            this.listeCtrller = new ListeETUController();
            this.secretaireCtrller = new SecretaireController(this);
            this.notifCtrler = new NotificationController(this);
        }

        private void listeEtudiants_Click(object sender, EventArgs e)
        {
            this.tabPage1.Show();
            this.tabPrincipal.SelectedIndex = 0;
            this.listeCtrller.getAllClass(this);
        }

        private void listeProfesseur_Click(object sender, EventArgs e)
        {
            this.tabPage2.Show();
            this.tabPrincipal.SelectedIndex = 1;
            this.secretaireCtrller.insertionDonneeListeProf();
        }

        private void listeCours_Click(object sender, EventArgs e)
        {
            this.tabPage3.Show();
            this.tabPrincipal.SelectedIndex = 2;
            this.notifCtrler.insertionDonneeListeNotif();
        }

        private void sujetExamen_Click(object sender, EventArgs e)
        {
            //this.tabPage4.Show();
            //this.tabPrincipal.SelectedIndex = 3;
        }

        private void message_Click(object sender, EventArgs e)
        {
            this.tabPage4.Show();
            this.tabPrincipal.SelectedIndex = 3;
        }
        
        private void label1_Click(object sender, EventArgs e)
        {

        }


        public ComboBox getListeDeroulanteClasse()
        {
            return this.listeClasse;
        }
        public Label getNomClasseLabel()
        {
            return this.nomClasse;
        }
        public Label getNbreEtu()
        {
            return this.nbreETU;
        }
        public DataGridView getTableauListeETU()
        {
            return this.dataGridViewListeETU;
        }

        public DataGridView getListeProf()
        {
            return this.tableauListeProf;
        }

        private void listeClasse_SelectedIndexChanged(object sender, EventArgs e)
        {
            string value = ((KeyValuePair<int, string>)listeClasse.SelectedItem).Value;
            int key = ((KeyValuePair<int, string>)listeClasse.SelectedItem).Key;
            this.listeCtrller.idClasse = key;
            this.listeCtrller.getAllClasses2(this);
            this.nomClasse.Text = value;
            //Console.WriteLine("niova : " + value);
        }

        private void tabPrincipal_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.tabPage1.Focus())
            {
                listeEtudiants_Click(sender, e);
            }

            if (this.tabPage2.Focus())
            {
                this.secretaireCtrller.insertionDonneeListeProf();
            }

            if (this.tabPage3.Focus())
            {
                this.notifCtrler.insertionDonneeListeNotif();
            }

            if (this.tabPage4.Focus())
            {
                Console.WriteLine("tabpage 4");
            }
            if (this.tabPage5.Focus())
            {
                fonctionImage();
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.tabPage3.Show();
            this.tabPrincipal.SelectedIndex = 2;
        }

        public Label getNombreNotif()
        {
            return this.nbre_notif;
        }
        public Label getNombreNotifEnHaut()
        {
            return this.nbreNotif_en_Haut;
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            this.notifCtrler.getNbreNotifDuJour();
        }

        public DataGridView getTabListeNotif()
        {
            return this.listeDonneeNotif;
        }

        private void imageUser_Click(object sender, EventArgs e)
        {
            this.tabPage5.Show();
            this.tabPrincipal.SelectedIndex = 4;
            fonctionImage();
        }

        private void Secretaire_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.conx.Close();
        }

        public List<Label> getListeLabelProfil()
        {
            List<Label> result = new List<Label>();
            result.Add(labelTitreProfil);
            result.Add(valeurNom); result.Add(valeurPrenom);
            result.Add(valeurAdresse); result.Add(valeurEmail);
            result.Add(valeurPhone); result.Add(valeurPseudo);
            result.Add(valeurMdp);
            return result;
        }
        public PictureBox getImgUser()
        {
            return this.imgProfil;
        }
        public PictureBox getImgUser2()
        {
            return this.imageUser;
        }

        public Utilisateur getUtilisateur() {
            return this.user;
        }
        public Secretaires getSecretaires()
        {
            return this.secretR;
        }

        private void fonctionImage()
        {
            this.secretaireCtrller.affichageProfilUtilisateur();
            this.secretaireCtrller.afficherImageUser();
            this.labelTitreImg.Text = "Image de  profil de " + this.user.Prenom + " " + this.user.Nom;
        }
    }
}
