﻿namespace TPT
{
    partial class Secretaire
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Secretaire));
            this.panelContenu = new System.Windows.Forms.Panel();
            this.nbreNotif_en_Haut = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tabPrincipal = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.nbreETU = new System.Windows.Forms.Label();
            this.labelNbreETU = new System.Windows.Forms.Label();
            this.nomClasse = new System.Windows.Forms.Label();
            this.listeClasse = new System.Windows.Forms.ComboBox();
            this.dataGridViewListeETU = new System.Windows.Forms.DataGridView();
            this.Numero = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Prenom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.titre = new System.Windows.Forms.Label();
            this.tableauListeProf = new System.Windows.Forms.DataGridView();
            this.nomProf = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.prenomProf = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.adresseProf = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.nbre_notif = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.listeDonneeNotif = new System.Windows.Forms.DataGridView();
            this.destinateur = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.libelle = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateNotif = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nomDeClasse = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.labelTitreImg = new System.Windows.Forms.Label();
            this.valeurPseudo = new System.Windows.Forms.Label();
            this.valeurPhone = new System.Windows.Forms.Label();
            this.valeurEmail = new System.Windows.Forms.Label();
            this.valeurAdresse = new System.Windows.Forms.Label();
            this.valeurPrenom = new System.Windows.Forms.Label();
            this.valeurNom = new System.Windows.Forms.Label();
            this.labelMdp = new System.Windows.Forms.Label();
            this.labelPseudo = new System.Windows.Forms.Label();
            this.labelPhone = new System.Windows.Forms.Label();
            this.labelMail = new System.Windows.Forms.Label();
            this.labelAdresse = new System.Windows.Forms.Label();
            this.labelPrenom = new System.Windows.Forms.Label();
            this.labelNom = new System.Windows.Forms.Label();
            this.labelTitreProfil = new System.Windows.Forms.Label();
            this.imgProfil = new System.Windows.Forms.PictureBox();
            this.valeurMdp = new System.Windows.Forms.Label();
            this.panelMenu = new System.Windows.Forms.Panel();
            this.nomUser = new System.Windows.Forms.Label();
            this.imageUser = new System.Windows.Forms.PictureBox();
            this.listeEtudiants = new System.Windows.Forms.Label();
            this.message = new System.Windows.Forms.Label();
            this.listeCours = new System.Windows.Forms.Label();
            this.emploiDuTps = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panelContenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabPrincipal.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewListeETU)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tableauListeProf)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.listeDonneeNotif)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgProfil)).BeginInit();
            this.panelMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageUser)).BeginInit();
            this.SuspendLayout();
            // 
            // panelContenu
            // 
            this.panelContenu.Controls.Add(this.nbreNotif_en_Haut);
            this.panelContenu.Controls.Add(this.panel1);
            this.panelContenu.Controls.Add(this.pictureBox1);
            this.panelContenu.Controls.Add(this.tabPrincipal);
            this.panelContenu.Location = new System.Drawing.Point(178, 3);
            this.panelContenu.Name = "panelContenu";
            this.panelContenu.Size = new System.Drawing.Size(1045, 484);
            this.panelContenu.TabIndex = 0;
            // 
            // nbreNotif_en_Haut
            // 
            this.nbreNotif_en_Haut.AutoSize = true;
            this.nbreNotif_en_Haut.BackColor = System.Drawing.Color.Transparent;
            this.nbreNotif_en_Haut.Cursor = System.Windows.Forms.Cursors.Hand;
            this.nbreNotif_en_Haut.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nbreNotif_en_Haut.ForeColor = System.Drawing.Color.Red;
            this.nbreNotif_en_Haut.Location = new System.Drawing.Point(998, 0);
            this.nbreNotif_en_Haut.Name = "nbreNotif_en_Haut";
            this.nbreNotif_en_Haut.Size = new System.Drawing.Size(17, 17);
            this.nbreNotif_en_Haut.TabIndex = 5;
            this.nbreNotif_en_Haut.Text = "6";
            this.nbreNotif_en_Haut.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Location = new System.Drawing.Point(970, 37);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(73, 440);
            this.panel1.TabIndex = 5;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(983, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(34, 38);
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // tabPrincipal
            // 
            this.tabPrincipal.Controls.Add(this.tabPage1);
            this.tabPrincipal.Controls.Add(this.tabPage2);
            this.tabPrincipal.Controls.Add(this.tabPage3);
            this.tabPrincipal.Controls.Add(this.tabPage4);
            this.tabPrincipal.Controls.Add(this.tabPage5);
            this.tabPrincipal.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPrincipal.ItemSize = new System.Drawing.Size(90, 30);
            this.tabPrincipal.Location = new System.Drawing.Point(3, 3);
            this.tabPrincipal.Name = "tabPrincipal";
            this.tabPrincipal.SelectedIndex = 0;
            this.tabPrincipal.Size = new System.Drawing.Size(974, 478);
            this.tabPrincipal.TabIndex = 2;
            this.tabPrincipal.SelectedIndexChanged += new System.EventHandler(this.tabPrincipal_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.nbreETU);
            this.tabPage1.Controls.Add(this.labelNbreETU);
            this.tabPage1.Controls.Add(this.nomClasse);
            this.tabPage1.Controls.Add(this.listeClasse);
            this.tabPage1.Controls.Add(this.dataGridViewListeETU);
            this.tabPage1.Location = new System.Drawing.Point(4, 34);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(966, 440);
            this.tabPage1.TabIndex = 1;
            this.tabPage1.Text = "Liste des étudiants";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // nbreETU
            // 
            this.nbreETU.AutoSize = true;
            this.nbreETU.Location = new System.Drawing.Point(765, 28);
            this.nbreETU.Name = "nbreETU";
            this.nbreETU.Size = new System.Drawing.Size(27, 20);
            this.nbreETU.TabIndex = 4;
            this.nbreETU.Text = "25";
            // 
            // labelNbreETU
            // 
            this.labelNbreETU.AutoSize = true;
            this.labelNbreETU.Location = new System.Drawing.Point(606, 27);
            this.labelNbreETU.Name = "labelNbreETU";
            this.labelNbreETU.Size = new System.Drawing.Size(159, 20);
            this.labelNbreETU.TabIndex = 3;
            this.labelNbreETU.Text = "Nombre d\'étudiants : ";
            // 
            // nomClasse
            // 
            this.nomClasse.AutoSize = true;
            this.nomClasse.Location = new System.Drawing.Point(471, 26);
            this.nomClasse.Name = "nomClasse";
            this.nomClasse.Size = new System.Drawing.Size(70, 20);
            this.nomClasse.TabIndex = 2;
            this.nomClasse.Text = "Classe 1";
            this.nomClasse.Click += new System.EventHandler(this.label1_Click);
            // 
            // listeClasse
            // 
            this.listeClasse.FormattingEnabled = true;
            this.listeClasse.Items.AddRange(new object[] {
            "Classe 1",
            "Classe 2",
            "Classe 3"});
            this.listeClasse.Location = new System.Drawing.Point(93, 21);
            this.listeClasse.Name = "listeClasse";
            this.listeClasse.Size = new System.Drawing.Size(262, 28);
            this.listeClasse.TabIndex = 1;
            this.listeClasse.SelectedIndexChanged += new System.EventHandler(this.listeClasse_SelectedIndexChanged);
            // 
            // dataGridViewListeETU
            // 
            this.dataGridViewListeETU.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewListeETU.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Numero,
            this.Nom,
            this.Prenom});
            this.dataGridViewListeETU.Location = new System.Drawing.Point(6, 76);
            this.dataGridViewListeETU.Name = "dataGridViewListeETU";
            this.dataGridViewListeETU.Size = new System.Drawing.Size(944, 297);
            this.dataGridViewListeETU.TabIndex = 0;
            // 
            // Numero
            // 
            this.Numero.HeaderText = "Numero";
            this.Numero.Name = "Numero";
            // 
            // Nom
            // 
            this.Nom.HeaderText = "Nom";
            this.Nom.Name = "Nom";
            this.Nom.Width = 300;
            // 
            // Prenom
            // 
            this.Prenom.HeaderText = "Prenom";
            this.Prenom.Name = "Prenom";
            this.Prenom.Width = 500;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.titre);
            this.tabPage2.Controls.Add(this.tableauListeProf);
            this.tabPage2.Location = new System.Drawing.Point(4, 34);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(966, 440);
            this.tabPage2.TabIndex = 0;
            this.tabPage2.Text = "Liste des professeurs";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // titre
            // 
            this.titre.AutoSize = true;
            this.titre.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.titre.Location = new System.Drawing.Point(263, 22);
            this.titre.Name = "titre";
            this.titre.Size = new System.Drawing.Size(241, 20);
            this.titre.TabIndex = 1;
            this.titre.Text = "Liste de tous les professeurs";
            // 
            // tableauListeProf
            // 
            this.tableauListeProf.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tableauListeProf.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nomProf,
            this.prenomProf,
            this.adresseProf});
            this.tableauListeProf.Location = new System.Drawing.Point(12, 57);
            this.tableauListeProf.Name = "tableauListeProf";
            this.tableauListeProf.Size = new System.Drawing.Size(1013, 377);
            this.tableauListeProf.TabIndex = 0;
            // 
            // nomProf
            // 
            this.nomProf.HeaderText = "Nom";
            this.nomProf.Name = "nomProf";
            this.nomProf.Width = 300;
            // 
            // prenomProf
            // 
            this.prenomProf.HeaderText = "Prénom.s";
            this.prenomProf.Name = "prenomProf";
            this.prenomProf.Width = 300;
            // 
            // adresseProf
            // 
            this.adresseProf.HeaderText = "Adresse";
            this.adresseProf.Name = "adresseProf";
            this.adresseProf.Width = 600;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.nbre_notif);
            this.tabPage3.Controls.Add(this.label1);
            this.tabPage3.Controls.Add(this.listeDonneeNotif);
            this.tabPage3.Controls.Add(this.nomDeClasse);
            this.tabPage3.Location = new System.Drawing.Point(4, 34);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(966, 440);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Liste notifications";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // nbre_notif
            // 
            this.nbre_notif.AutoSize = true;
            this.nbre_notif.Location = new System.Drawing.Point(717, 28);
            this.nbre_notif.Name = "nbre_notif";
            this.nbre_notif.Size = new System.Drawing.Size(27, 20);
            this.nbre_notif.TabIndex = 4;
            this.nbre_notif.Text = "20";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(585, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(134, 20);
            this.label1.TabIndex = 3;
            this.label1.Text = "Nombre de notif : ";
            // 
            // listeDonneeNotif
            // 
            this.listeDonneeNotif.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.listeDonneeNotif.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.destinateur,
            this.libelle,
            this.dateNotif});
            this.listeDonneeNotif.Location = new System.Drawing.Point(6, 66);
            this.listeDonneeNotif.Name = "listeDonneeNotif";
            this.listeDonneeNotif.Size = new System.Drawing.Size(957, 352);
            this.listeDonneeNotif.TabIndex = 2;
            // 
            // destinateur
            // 
            this.destinateur.HeaderText = "Destinateur";
            this.destinateur.Name = "destinateur";
            this.destinateur.Width = 300;
            // 
            // libelle
            // 
            this.libelle.HeaderText = "Libellé";
            this.libelle.Name = "libelle";
            this.libelle.Width = 500;
            // 
            // dateNotif
            // 
            this.dateNotif.HeaderText = "Date";
            this.dateNotif.Name = "dateNotif";
            this.dateNotif.Width = 150;
            // 
            // nomDeClasse
            // 
            this.nomDeClasse.AutoSize = true;
            this.nomDeClasse.Location = new System.Drawing.Point(344, 28);
            this.nomDeClasse.Name = "nomDeClasse";
            this.nomDeClasse.Size = new System.Drawing.Size(162, 20);
            this.nomDeClasse.TabIndex = 0;
            this.nomDeClasse.Text = "Liste des notifications";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.label3);
            this.tabPage4.Controls.Add(this.label4);
            this.tabPage4.Controls.Add(this.dataGridView2);
            this.tabPage4.Controls.Add(this.comboBox1);
            this.tabPage4.Controls.Add(this.label5);
            this.tabPage4.Location = new System.Drawing.Point(4, 34);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(966, 440);
            this.tabPage4.TabIndex = 4;
            this.tabPage4.Text = "Messages";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(846, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(27, 20);
            this.label3.TabIndex = 14;
            this.label3.Text = "20";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(676, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(176, 20);
            this.label4.TabIndex = 13;
            this.label4.Text = "Nombre de messages : ";
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5});
            this.dataGridView2.Location = new System.Drawing.Point(20, 77);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(842, 331);
            this.dataGridView2.TabIndex = 12;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Destinateur";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "Objet";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Width = 200;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "Contenu";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Width = 500;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Classe 1",
            "Classe 2",
            "Classe 3"});
            this.comboBox1.Location = new System.Drawing.Point(274, 23);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(157, 28);
            this.comboBox1.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(517, 26);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 20);
            this.label5.TabIndex = 10;
            this.label5.Text = "Classe 1";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.labelTitreImg);
            this.tabPage5.Controls.Add(this.valeurPseudo);
            this.tabPage5.Controls.Add(this.valeurPhone);
            this.tabPage5.Controls.Add(this.valeurEmail);
            this.tabPage5.Controls.Add(this.valeurAdresse);
            this.tabPage5.Controls.Add(this.valeurPrenom);
            this.tabPage5.Controls.Add(this.valeurNom);
            this.tabPage5.Controls.Add(this.labelMdp);
            this.tabPage5.Controls.Add(this.labelPseudo);
            this.tabPage5.Controls.Add(this.labelPhone);
            this.tabPage5.Controls.Add(this.labelMail);
            this.tabPage5.Controls.Add(this.labelAdresse);
            this.tabPage5.Controls.Add(this.labelPrenom);
            this.tabPage5.Controls.Add(this.labelNom);
            this.tabPage5.Controls.Add(this.labelTitreProfil);
            this.tabPage5.Controls.Add(this.imgProfil);
            this.tabPage5.Controls.Add(this.valeurMdp);
            this.tabPage5.Location = new System.Drawing.Point(4, 34);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(966, 440);
            this.tabPage5.TabIndex = 5;
            this.tabPage5.Text = "Mon profil";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // labelTitreImg
            // 
            this.labelTitreImg.AutoSize = true;
            this.labelTitreImg.Location = new System.Drawing.Point(192, 329);
            this.labelTitreImg.Name = "labelTitreImg";
            this.labelTitreImg.Size = new System.Drawing.Size(51, 20);
            this.labelTitreImg.TabIndex = 33;
            this.labelTitreImg.Text = "label6";
            // 
            // valeurPseudo
            // 
            this.valeurPseudo.AutoSize = true;
            this.valeurPseudo.Location = new System.Drawing.Point(684, 318);
            this.valeurPseudo.Name = "valeurPseudo";
            this.valeurPseudo.Size = new System.Drawing.Size(60, 20);
            this.valeurPseudo.TabIndex = 31;
            this.valeurPseudo.Text = "label11";
            // 
            // valeurPhone
            // 
            this.valeurPhone.AutoSize = true;
            this.valeurPhone.Location = new System.Drawing.Point(684, 278);
            this.valeurPhone.Name = "valeurPhone";
            this.valeurPhone.Size = new System.Drawing.Size(60, 20);
            this.valeurPhone.TabIndex = 30;
            this.valeurPhone.Text = "label10";
            // 
            // valeurEmail
            // 
            this.valeurEmail.AutoSize = true;
            this.valeurEmail.Location = new System.Drawing.Point(684, 238);
            this.valeurEmail.Name = "valeurEmail";
            this.valeurEmail.Size = new System.Drawing.Size(51, 20);
            this.valeurEmail.TabIndex = 29;
            this.valeurEmail.Text = "label9";
            // 
            // valeurAdresse
            // 
            this.valeurAdresse.AutoSize = true;
            this.valeurAdresse.Location = new System.Drawing.Point(684, 198);
            this.valeurAdresse.Name = "valeurAdresse";
            this.valeurAdresse.Size = new System.Drawing.Size(51, 20);
            this.valeurAdresse.TabIndex = 28;
            this.valeurAdresse.Text = "label8";
            // 
            // valeurPrenom
            // 
            this.valeurPrenom.AutoSize = true;
            this.valeurPrenom.Location = new System.Drawing.Point(684, 158);
            this.valeurPrenom.Name = "valeurPrenom";
            this.valeurPrenom.Size = new System.Drawing.Size(51, 20);
            this.valeurPrenom.TabIndex = 27;
            this.valeurPrenom.Text = "label7";
            // 
            // valeurNom
            // 
            this.valeurNom.AutoSize = true;
            this.valeurNom.Location = new System.Drawing.Point(684, 118);
            this.valeurNom.Name = "valeurNom";
            this.valeurNom.Size = new System.Drawing.Size(51, 20);
            this.valeurNom.TabIndex = 26;
            this.valeurNom.Text = "label6";
            // 
            // labelMdp
            // 
            this.labelMdp.AutoSize = true;
            this.labelMdp.Location = new System.Drawing.Point(531, 358);
            this.labelMdp.Name = "labelMdp";
            this.labelMdp.Size = new System.Drawing.Size(117, 20);
            this.labelMdp.TabIndex = 25;
            this.labelMdp.Text = "Mot de passe : ";
            // 
            // labelPseudo
            // 
            this.labelPseudo.AutoSize = true;
            this.labelPseudo.Location = new System.Drawing.Point(531, 318);
            this.labelPseudo.Name = "labelPseudo";
            this.labelPseudo.Size = new System.Drawing.Size(75, 20);
            this.labelPseudo.TabIndex = 24;
            this.labelPseudo.Text = "Pseudo : ";
            // 
            // labelPhone
            // 
            this.labelPhone.AutoSize = true;
            this.labelPhone.Location = new System.Drawing.Point(531, 278);
            this.labelPhone.Name = "labelPhone";
            this.labelPhone.Size = new System.Drawing.Size(96, 20);
            this.labelPhone.TabIndex = 23;
            this.labelPhone.Text = "Téléphone : ";
            // 
            // labelMail
            // 
            this.labelMail.AutoSize = true;
            this.labelMail.Location = new System.Drawing.Point(531, 238);
            this.labelMail.Name = "labelMail";
            this.labelMail.Size = new System.Drawing.Size(48, 20);
            this.labelMail.TabIndex = 22;
            this.labelMail.Text = "Email";
            // 
            // labelAdresse
            // 
            this.labelAdresse.AutoSize = true;
            this.labelAdresse.Location = new System.Drawing.Point(531, 198);
            this.labelAdresse.Name = "labelAdresse";
            this.labelAdresse.Size = new System.Drawing.Size(80, 20);
            this.labelAdresse.TabIndex = 21;
            this.labelAdresse.Text = "Adresse : ";
            // 
            // labelPrenom
            // 
            this.labelPrenom.AutoSize = true;
            this.labelPrenom.Location = new System.Drawing.Point(531, 158);
            this.labelPrenom.Name = "labelPrenom";
            this.labelPrenom.Size = new System.Drawing.Size(72, 20);
            this.labelPrenom.TabIndex = 20;
            this.labelPrenom.Text = "Prénom :";
            // 
            // labelNom
            // 
            this.labelNom.AutoSize = true;
            this.labelNom.Location = new System.Drawing.Point(531, 118);
            this.labelNom.Name = "labelNom";
            this.labelNom.Size = new System.Drawing.Size(54, 20);
            this.labelNom.TabIndex = 19;
            this.labelNom.Text = "Nom : ";
            // 
            // labelTitreProfil
            // 
            this.labelTitreProfil.AutoSize = true;
            this.labelTitreProfil.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTitreProfil.Location = new System.Drawing.Point(453, 51);
            this.labelTitreProfil.Name = "labelTitreProfil";
            this.labelTitreProfil.Size = new System.Drawing.Size(66, 24);
            this.labelTitreProfil.TabIndex = 18;
            this.labelTitreProfil.Text = "label6";
            // 
            // imgProfil
            // 
            this.imgProfil.Location = new System.Drawing.Point(184, 128);
            this.imgProfil.Name = "imgProfil";
            this.imgProfil.Size = new System.Drawing.Size(188, 175);
            this.imgProfil.TabIndex = 17;
            this.imgProfil.TabStop = false;
            // 
            // valeurMdp
            // 
            this.valeurMdp.AutoSize = true;
            this.valeurMdp.Font = new System.Drawing.Font("Microsoft Sans Serif", 56F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.valeurMdp.Location = new System.Drawing.Point(664, 305);
            this.valeurMdp.Name = "valeurMdp";
            this.valeurMdp.Size = new System.Drawing.Size(100, 85);
            this.valeurMdp.TabIndex = 32;
            this.valeurMdp.Text = "...";
            // 
            // panelMenu
            // 
            this.panelMenu.Controls.Add(this.nomUser);
            this.panelMenu.Controls.Add(this.imageUser);
            this.panelMenu.Controls.Add(this.listeEtudiants);
            this.panelMenu.Controls.Add(this.message);
            this.panelMenu.Controls.Add(this.listeCours);
            this.panelMenu.Controls.Add(this.emploiDuTps);
            this.panelMenu.Location = new System.Drawing.Point(1, 3);
            this.panelMenu.Name = "panelMenu";
            this.panelMenu.Size = new System.Drawing.Size(171, 455);
            this.panelMenu.TabIndex = 0;
            // 
            // nomUser
            // 
            this.nomUser.AutoSize = true;
            this.nomUser.Cursor = System.Windows.Forms.Cursors.Hand;
            this.nomUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nomUser.Location = new System.Drawing.Point(16, 171);
            this.nomUser.Name = "nomUser";
            this.nomUser.Size = new System.Drawing.Size(79, 17);
            this.nomUser.TabIndex = 0;
            this.nomUser.Text = "Nom User";
            this.nomUser.Click += new System.EventHandler(this.imageUser_Click);
            // 
            // imageUser
            // 
            this.imageUser.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imageUser.Image = ((System.Drawing.Image)(resources.GetObject("imageUser.Image")));
            this.imageUser.Location = new System.Drawing.Point(11, 3);
            this.imageUser.Name = "imageUser";
            this.imageUser.Size = new System.Drawing.Size(151, 144);
            this.imageUser.TabIndex = 0;
            this.imageUser.TabStop = false;
            this.imageUser.Click += new System.EventHandler(this.imageUser_Click);
            // 
            // listeEtudiants
            // 
            this.listeEtudiants.AutoSize = true;
            this.listeEtudiants.Cursor = System.Windows.Forms.Cursors.Hand;
            this.listeEtudiants.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listeEtudiants.Location = new System.Drawing.Point(4, 220);
            this.listeEtudiants.Name = "listeEtudiants";
            this.listeEtudiants.Size = new System.Drawing.Size(143, 20);
            this.listeEtudiants.TabIndex = 1;
            this.listeEtudiants.Text = "Liste des étudiants";
            this.listeEtudiants.Click += new System.EventHandler(this.listeEtudiants_Click);
            // 
            // message
            // 
            this.message.AutoSize = true;
            this.message.Cursor = System.Windows.Forms.Cursors.Hand;
            this.message.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.message.Location = new System.Drawing.Point(3, 370);
            this.message.Name = "message";
            this.message.Size = new System.Drawing.Size(82, 20);
            this.message.TabIndex = 4;
            this.message.Text = "Messages";
            this.message.Click += new System.EventHandler(this.message_Click);
            // 
            // listeCours
            // 
            this.listeCours.AutoSize = true;
            this.listeCours.Cursor = System.Windows.Forms.Cursors.Hand;
            this.listeCours.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listeCours.Location = new System.Drawing.Point(3, 320);
            this.listeCours.Name = "listeCours";
            this.listeCours.Size = new System.Drawing.Size(124, 20);
            this.listeCours.TabIndex = 2;
            this.listeCours.Text = "Liste notification";
            this.listeCours.Click += new System.EventHandler(this.listeCours_Click);
            // 
            // emploiDuTps
            // 
            this.emploiDuTps.AutoSize = true;
            this.emploiDuTps.Cursor = System.Windows.Forms.Cursors.Hand;
            this.emploiDuTps.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.emploiDuTps.Location = new System.Drawing.Point(3, 270);
            this.emploiDuTps.Name = "emploiDuTps";
            this.emploiDuTps.Size = new System.Drawing.Size(161, 20);
            this.emploiDuTps.TabIndex = 0;
            this.emploiDuTps.Text = "Liste des professeurs";
            this.emploiDuTps.Click += new System.EventHandler(this.listeProfesseur_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 5000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Secretaire
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1223, 486);
            this.Controls.Add(this.panelMenu);
            this.Controls.Add(this.panelContenu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "Secretaire";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Secrétaires";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Secretaire_FormClosed);
            this.panelContenu.ResumeLayout(false);
            this.panelContenu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabPrincipal.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewListeETU)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tableauListeProf)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.listeDonneeNotif)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgProfil)).EndInit();
            this.panelMenu.ResumeLayout(false);
            this.panelMenu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageUser)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelContenu;
        private System.Windows.Forms.Panel panelMenu;
        private System.Windows.Forms.Label emploiDuTps;
        private System.Windows.Forms.Label message;
        private System.Windows.Forms.Label listeCours;
        private System.Windows.Forms.Label listeEtudiants;
        private System.Windows.Forms.Label nomUser;
        private System.Windows.Forms.PictureBox imageUser;
        private System.Windows.Forms.Label titre;
        private System.Windows.Forms.DataGridView tableauListeProf;
        private System.Windows.Forms.TabControl tabPrincipal;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.ComboBox listeClasse;
        private System.Windows.Forms.DataGridView dataGridViewListeETU;
        private System.Windows.Forms.DataGridViewTextBoxColumn Numero;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nom;
        private System.Windows.Forms.DataGridViewTextBoxColumn Prenom;
        private System.Windows.Forms.Label nomClasse;
        private System.Windows.Forms.Label nbreETU;
        private System.Windows.Forms.Label labelNbreETU;
        private System.Windows.Forms.DataGridView listeDonneeNotif;
        private System.Windows.Forms.Label nomDeClasse;
        private System.Windows.Forms.Label nbre_notif;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomProf;
        private System.Windows.Forms.DataGridViewTextBoxColumn prenomProf;
        private System.Windows.Forms.DataGridViewTextBoxColumn adresseProf;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label nbreNotif_en_Haut;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.DataGridViewTextBoxColumn destinateur;
        private System.Windows.Forms.DataGridViewTextBoxColumn libelle;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateNotif;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Label labelTitreImg;
        private System.Windows.Forms.Label valeurPseudo;
        private System.Windows.Forms.Label valeurPhone;
        private System.Windows.Forms.Label valeurEmail;
        private System.Windows.Forms.Label valeurAdresse;
        private System.Windows.Forms.Label valeurPrenom;
        private System.Windows.Forms.Label valeurNom;
        private System.Windows.Forms.Label labelMdp;
        private System.Windows.Forms.Label labelPseudo;
        private System.Windows.Forms.Label labelPhone;
        private System.Windows.Forms.Label labelMail;
        private System.Windows.Forms.Label labelAdresse;
        private System.Windows.Forms.Label labelPrenom;
        private System.Windows.Forms.Label labelNom;
        private System.Windows.Forms.Label labelTitreProfil;
        private System.Windows.Forms.PictureBox imgProfil;
        private System.Windows.Forms.Label valeurMdp;
    }
}