﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TPT.model;

namespace TPT.controller
{
    class ProfesseurController : BaseController
    {
        Professeur prof;
        public ProfesseurController(Professeur p)
        {
            this.prof = p;
        }

        public void affichageProfilUtilisateur()
        {
            Utilisateur user = this.prof.getUtilisateur();
            Professeurs profs = this.prof.getProfesseurs();
            List<Label> listeProfil = this.prof.getListeLabelProfil();
            listeProfil[0].Text = "Profil de " + user.Prenom + " " + user.Nom;
            listeProfil[1].Text = user.Nom;
            listeProfil[2].Text = user.Prenom;
            listeProfil[3].Text = user.Adresse;
            listeProfil[4].Text = user.Mail;
            listeProfil[5].Text = profs.Phone;
            listeProfil[6].Text = user.Login;
            //listeProfil[7].Text = user.Mdp;
        }

        public void afficherImageUser()
        {
            PictureBox img = this.prof.getImgUser();
            string imgProfil = this.prof.getProfesseurs().Photo;
            //Console.WriteLine(this.prof.getProfesseurs().Photo);
            img.Load(@"../../Assets/images/" + imgProfil);
        }
        public void afficherImgProfil()
        {
            PictureBox img2 = this.prof.getImgUser2();
            string imgProfil = this.prof.getProfesseurs().Photo;
            img2.Load(@"../../Assets/images/" + imgProfil);
        }
    }
}
