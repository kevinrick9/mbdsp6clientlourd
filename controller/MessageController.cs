﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TPT.model;

namespace TPT.controller
{
    class MessageController : BaseController
    {
        Professeur p;
        public void insertionDonneeMsg(Professeur prof)
        {
            this.p = prof;
            List<Messages> listeMsg = notifSvce.getMessageByIdDest(this.p.getProfesseurs().IdProf);
            notifSvce.insertDonneListeMsgBYIdDest(listeMsg, this.p.getTableauListeMsg());
            this.p.getNbreMsgGot().Text = listeMsg.Count().ToString();
        }
    }
}
