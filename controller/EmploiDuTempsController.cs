﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TPT.model;

namespace TPT.controller
{
    class EmploiDuTempsController : BaseController
    {
        Professeur prof;
        public EmploiDuTempsController(Professeur p)
        {
            this.prof = p;
        }
        public void getAllEmploiByIdUser()
        {
            Professeurs profs = this.prof.getProfesseurs();
            List<EmploiDuTemps> listeEmploiDuTemps = emploiDuTempsSvce.getEmploiDuTempsByIdUser(profs.IdProf);
            emploiDuTempsSvce.insertDonneListeEmploiDuTemps(listeEmploiDuTemps,this.prof.getTableauEmploiDuTemps());
            List<string> intervalleDate = new List<string>();
            try
            {
                intervalleDate = emploiDuTempsSvce.getDateDebutEtFinSemaine(listeEmploiDuTemps[0].DateMatiere);
            }
            catch
            {

            }
            this.prof.getTitreEmploiDuTempsProfs().Text = "Mon emploi du temps pour la semaine du "+intervalleDate+" au "+intervalleDate;
        }
    }
}
