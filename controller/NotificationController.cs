﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TPT.model;

namespace TPT.controller
{
    class NotificationController : BaseController
    {
        Secretaire sec;
        public NotificationController(Secretaire s)
        {
            this.sec = s;
        }
        public void insertionDonneeListeNotif()
        {
            List<Notification> listeNotification = notifSvce.getAllNotifFromAPi();
            int tailleNotif = listeNotification.Count();
            this.sec.getNombreNotif().Text = tailleNotif.ToString();
            notifSvce.insertDonneListeNotif(listeNotification, this.sec.getTabListeNotif());
        }
        public void getNbreNotifDuJour()
        {
            string dateActuelle = utilSvce.dateTimeToString(utilSvce.getDateActuelle());
            List<Notification> listeNotification = notifSvce.getAllNotifByDateFromAPi(dateActuelle);
            int taille = listeNotification.Count();
            if (taille == 0)
            {
                this.sec.getNombreNotifEnHaut().Visible = false;
            }
            else
            {
                this.sec.getNombreNotifEnHaut().Visible = true;
                this.sec.getNombreNotifEnHaut().Text = taille.ToString();
            }
        }
    }
}
