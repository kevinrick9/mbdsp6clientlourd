﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TPT.model;

namespace TPT.controller
{
    class ListeETUController : BaseController
    {
        Professeur p;
        Secretaire s;
        private int idClasses;
        public int idClasse = 1;
        public int IdClasses
        {
            get { return idClasses; }
            set { idClasses = value; }
        }

        public void userProfile(Professeur rp)
        {
            this.p = rp;
        }
        public void getAllClass(Professeur profScreen)
        {
            this.p = profScreen;
            List<Classe> listeClass = classSvce.getAllClasses();
            
            //int idClasse = listeClass[0].IdClasse;
            List<Eleve> listeETU_Classe = classSvce.getEleveByIdClass(idClasse);
            int nbreEtu = listeETU_Classe.Count();
            //Console.WriteLine("l'id de la classe : "+idClasse);

            Dictionary<int, string> dictionnaire = new Dictionary<int, string>();
            foreach (Classe c in listeClass)
            {
                dictionnaire.Add(c.IdClasse, c.NomClasse);
            }
            //dataGridView1.Columns.Clear()
            //this.p.getListeDeroulanteClasse()
            this.p.getListeDeroulanteClasse().DataSource = new BindingSource(dictionnaire, null);
            this.p.getListeDeroulanteClasse().DisplayMember = "Value";
            this.p.getListeDeroulanteClasse().ValueMember = "Key";

            //this.p.getListeDeroulanteClasse().DataSource = nomClasse;
            this.p.getNomClasseLabel().Text = listeClass[0].NomClasse;
            this.p.getNbreEtu().Text = nbreEtu.ToString();
            //this.p.getTableauListeETU().DataSource
            classSvce.insertDonneListeETU(listeETU_Classe, this.p.getTableauListeETU());
        }

        public void getAllClasses2(Professeur prof)
        {
            this.p = prof;
            List<Classe> listeClass = classSvce.getAllClasses();

            //int idClasse = listeClass[0].IdClasse;
            List<Eleve> listeETU_Classe = classSvce.getEleveByIdClass(idClasse);
            int nbreEtu = listeETU_Classe.Count();
            this.p.getNbreEtu().Text = nbreEtu.ToString();
             classSvce.insertDonneListeETU(listeETU_Classe, this.p.getTableauListeETU());
        }


        public void getAllClass(Secretaire secretr)
        {
            this.s = secretr;
            List<Classe> listeClass = classSvce.getAllClasses();

            //int idClasse = listeClass[0].IdClasse;
            List<Eleve> listeETU_Classe = classSvce.getEleveByIdClass(idClasse);
            int nbreEtu = listeETU_Classe.Count();
            //Console.WriteLine("l'id de la classe : "+idClasse);

            Dictionary<int, string> dictionnaire = new Dictionary<int, string>();
            foreach (Classe c in listeClass)
            {
                dictionnaire.Add(c.IdClasse, c.NomClasse);
            }
            //dataGridView1.Columns.Clear()
            //this.p.getListeDeroulanteClasse()
            this.s.getListeDeroulanteClasse().DataSource = new BindingSource(dictionnaire, null);
            this.s.getListeDeroulanteClasse().DisplayMember = "Value";
            this.s.getListeDeroulanteClasse().ValueMember = "Key";

            //this.p.getListeDeroulanteClasse().DataSource = nomClasse;
            this.s.getNomClasseLabel().Text = listeClass[0].NomClasse;
            this.s.getNbreEtu().Text = nbreEtu.ToString();
            //this.p.getTableauListeETU().DataSource
            classSvce.insertDonneListeETU(listeETU_Classe, this.s.getTableauListeETU());
        }

        public void getAllClasses2(Secretaire sec)
        {
            this.s = sec;
            List<Classe> listeClass = classSvce.getAllClasses();

            //int idClasse = listeClass[0].IdClasse;
            List<Eleve> listeETU_Classe = classSvce.getEleveByIdClass(idClasse);
            int nbreEtu = listeETU_Classe.Count();
            this.s.getNbreEtu().Text = nbreEtu.ToString();
            classSvce.insertDonneListeETU(listeETU_Classe, this.s.getTableauListeETU());
        }
    }
}
