﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TPT.service;
using TPT.service.exception;

namespace TPT.controller
{
    class BaseController
    {
        public UtilisateurService userService = new UtilisateurService();
        public ClasseService classSvce = new ClasseService();
        public CoursService coursSvce = new CoursService();
        public SujetService sujetSvce = new SujetService();
        public UtilsService utilSvce = new UtilsService();
        public ProfesseurService profSvce = new ProfesseurService();
        public NotificationService notifSvce = new NotificationService();
        public EmploiDuTempsService emploiDuTempsSvce = new EmploiDuTempsService();
        public MatiereService matiereSvce = new MatiereService();
    }
}
