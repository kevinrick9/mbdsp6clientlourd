﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using TPT.model;
using TPT.service;
using TPT.service.exception;

namespace TPT.controller
{
    class AuthentificationController : BaseController
    {
        connexion affichageConnex; 
        public void authentification(string login, string mdp, connexion con)
        {
            this.affichageConnex = con;
            //postAuth();
            classSvce.getClasseFromAPi();
            try
            {
                Utilisateur user = userService.getUser(login, mdp);
                switch (user.Niveau)
                {
                    case 1: 
                        Professeurs prof = userService.getProf(user);
                        new Professeur(prof, user, this.affichageConnex).Show();
                        
                        break;
                    case 2: 
                        Secretaires sec = userService.getSec(user);
                        Secretaire s = new Secretaire(sec, user, this.affichageConnex);
                        s.Show();
                        //this.affichageConnex.Hide();
                        break;
                }
            }
            catch (UtilisateurException ex)
            {
                Console.WriteLine(ex.Msg);
                connexion c = new connexion();
                c.Message.Text = ex.Msg;
                c.Message.Visible = true;
                c.Show();
            }
        }
        
        public void getFiliere()
        {
            /*var client = new RestClient("http://localhost:8092/api/filiere/");
            var request = new RestRequest(Method.GET);
            request.AddHeader("X-Token-Key", "dsds-sdsdsds-swrwerfd-dfdfd");
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            dynamic json = JsonConvert.DeserializeObject(content);
            JObject customerObjJson = json.CustomerObj;
            var customerObj = customerObjJson.ToObject<Filiere>();
            Console.WriteLine("dans la fonction getfiliere\nl'identifiant du customer est : "+customerObj.IdFiliere);
            return customerObj;*/

            /*var client = new WebClient();
            client.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
            var response = client.DownloadString("http://localhost:8092/api/filiere/");
            var releases = JArray.Parse(response);
            Console.WriteLine("les resultats : "+releases);*/

            string url = Util.getUrl()+"filiere";
            var client = new RestClient(url);
            IRestResponse response = client.Execute(new RestRequest());
            var content = response.Content;
            var releases = JArray.Parse(content);
            /*dynamic json = JsonConvert.DeserializeObject(content);
            JObject customerObjJson = json.CustomerObj;*/
            Console.WriteLine("resultat : " + releases);
            //var releases = JArray.Parse(response.Content);
        }

        public void postAuth()
        {
            string url = Util.getUrl()+"utilisateur/login";
            var client = new RestClient(url);
            JObject jObjectbody = new JObject();
            jObjectbody.Add("pseudos", "Kev");
            jObjectbody.Add("mdps", "kevin");
            RestRequest restRequest = new RestRequest(Method.POST);
            //restRequest.AddJsonBody(jObjectbody);
            //restRequest.AddParameter();
            restRequest.AddParameter("application/x-www-form-urlencoded", jObjectbody, ParameterType.RequestBody);
            IRestResponse restResponse = client.Execute(restRequest);
            //Assert.Contains("OPERATION_SUCCESS ", restRequest.Body, " Post failed ");

            /*var request = new RestRequest(Method.POST);

            request.Resource = "url";
            request.RequestFormat = DataFormat.Json;

            request.AddJsonBody(jObjectbody);

            client.Execute(request);*/

        }


    }
}
