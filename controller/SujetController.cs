﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TPT.model;

namespace TPT.controller
{
    class SujetController : BaseController
    {
        Professeur p;

        public void getAllSujet(Professeur prof)
        {
            this.p = prof;
            Professeurs profs = this.p.getProfesseurs();
            List<Sujet> listeSujet = sujetSvce.getAllSujets(profs.IdProf);
            sujetSvce.insertDonneListeSujet(listeSujet, this.p.getTableauListeSujet());
        }
    }
}
