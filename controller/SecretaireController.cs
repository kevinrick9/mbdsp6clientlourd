﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TPT.model;

namespace TPT.controller
{
    class SecretaireController : BaseController
    {
        private Secretaire secretaire;

        public SecretaireController(Secretaire s){
            this.secretaire = s;
        }

        public void insertionDonneeListeProf(){
             List<Utilisateur> listeProfUser = userService.getUtilisateursProf();
             profSvce.insertDonneListeETU(listeProfUser, this.secretaire.getListeProf());
        }

        public void affichageProfilUtilisateur()
        {
            Utilisateur user = this.secretaire.getUtilisateur();
            Secretaires secs = this.secretaire.getSecretaires();
            List<Label> listeProfil = this.secretaire.getListeLabelProfil();
            listeProfil[0].Text = "Profil de " + user.Prenom + " " + user.Nom;
            listeProfil[1].Text = user.Nom;
            listeProfil[2].Text = user.Prenom;
            listeProfil[3].Text = user.Adresse;
            listeProfil[4].Text = user.Mail;
            listeProfil[5].Text = secs.Phone;
            listeProfil[6].Text = user.Login;
            //listeProfil[7].Text = user.Mdp;
        }


        public void afficherImageUser()
        {
            PictureBox img = this.secretaire.getImgUser();
            string imgProfil = this.secretaire.getSecretaires().Photo;
            //Console.WriteLine(this.prof.getProfesseurs().Photo);
            img.Load(@"../../Assets/images/" + imgProfil);
        }
        public void afficherImgProfil()
        {
            PictureBox img2 = this.secretaire.getImgUser2();
            string imgProfil = this.secretaire.getSecretaires().Photo;
            img2.Load(@"../../Assets/images/" + imgProfil);
        }
    }
}
