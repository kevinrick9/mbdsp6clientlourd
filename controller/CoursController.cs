﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TPT.model;

namespace TPT.controller
{
    class CoursController : BaseController
    {
        Professeur p;

        public void getAllCours(Professeur prof)
        {
            this.p = prof;
            Utilisateur u = this.p.getUtilisateur();
            List<Cours> listeCours = coursSvce.getCoursByIdProf(u.Id);

            coursSvce.insertDonneListeCours(listeCours, this.p.getTableauListeCours());
            /*Dictionary<int, string> dictionnaireListeCours = new Dictionary<int, string>();
            foreach (Cours c in listeCours)
            {
                dictionnaireListeCours.Add(c.IdCours, c.TitreCours);
            }
            this.p.getListeCours().DataSource = new BindingSource(dictionnaireListeCours, null);
            this.p.getListeCours().DisplayMember = "Value";
            this.p.getListeCours().ValueMember = "Key"; */

        }
    }
}
